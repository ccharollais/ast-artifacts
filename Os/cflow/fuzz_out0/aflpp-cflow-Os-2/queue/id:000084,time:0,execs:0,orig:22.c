extern void VerifyC(void);
#ifde_CXX
extern void VerifyCXX(void);
#endif
#include "VerifyFortran.h"
extern void VerifyFortran(void);

int main(void)
{
  VerifyC();
#ifdef VERIFY_CXX
  VerifyCXX();
#endif
  VerifyFortran();
  return 0;
}