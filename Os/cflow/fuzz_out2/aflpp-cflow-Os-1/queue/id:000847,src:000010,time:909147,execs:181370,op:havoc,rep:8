/* Implementation of the internal dcigettext function.
   Copyright (C) 1995-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* Tell glibc's <string.h> to provide a prototype for mempcpy().
   This must come before <config.h> because <config.h> may include
   <features.h>, and once <features.h> has been included, it's too late.  */
#ifndef _GNU_SOURCE
# define _GNU_SOURCE	1
#endif

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <sys/types.h>

#ifdef __GNUC__
# define alloca __builtin_alloca
# define HAVE_ALLOCA 1
#else
# ifdef _MSC_VER
#  include <malloc.h>
#  define alloca _alloca
# else
#  if defined HAVE_ALLOCA_H || defined _LIBC
#   include <alloca.h>
#  else
#   ifdef _AIX
 #pragma alloca
#   else
#    ifndef alloca
char *alloca ();
#    endif
#   endif
#  endif
# endif
#endif

#include <errno.h>
#ifndef errno
extern int errno;
#endif
#ifndef __set_errno
# define __set_errno(val) errno = (val)
#endif

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#if defined HAVE_UNISTD_H || defined _LIBC
# include <unistd.h>
#endif

#include <locale.h>

#ifdef _LIBC
  /* Guess whether integer division by zero raises signal SIGFPE.
     Set to 1 only if you know for sure.  In case of doubt, set to 0.  */
# if defined __alpha__ || defined __arm__ || defined __i386__ \
     || defined __m68k__ || defined __s390__
#  define INTDIV0_RAISES_SIGFPE 1
# else
#  define INTDIV0_RAISES_SIGFPE 0
# endif
#endif
#if !INTDIV0_RAISES_SIGFPE
# include <signal.h>
#endif

#if defined HAVE_SYS_PARAM_H || defined _LIBC
# include <sys/param.h>
#endif

#if !defined _LIBC
# include "localcharset.h"
#endif

#include "gettextP.h"
#include "plural-exp.h"
#ifdef _LIBC
# include <libintl.h>
#else
# ifdef IN_LIBGLOCALE
#  include <libintl.h>
# endif
# include "libgnuintl.h"
#endif
#include "hash-string.h"

/* Handle multi-threaded applications.  */
#ifdef _LIBC
# include <bits/libc-lock.h>
# define gl_rwlock_define_initialized __libc_rwlock_define_initialized
# define gl_rwlock_rdlock __libc_rwlock_rdlock
# define gl_rwlock_wrlock __libc_rwlock_wrlock
# define gl_rwlock_unlock __libc_rwlock_unlock
#else
# include "lock.h"
#endif

/* Alignment of types.  */
#if defined __GNUC__ && __GNUC__ >= 2
# define alignof(TYPE) __alignof__ (TYPE)
#else
# define alignof(TYPE) \
    ((int) &((struct { char dummy1; TYPE dummy2; } *) 0)->dummy2)
#endif

/* Some compilers, like SunOS4 cc, don't have offsetof in <stddef.h>.  */
#ifndef offsetof
# define offsetof(type,ident) ((size_t)&(((type*)0)->ident))
#endif

/* @@ end of prolog @@ */

#ifdef _LIBC
/* Rename the non ANSI C functions.  This is required by the standard
   because some ANSI C functions will require linking with this object
   file and the name space must not be polluted.  */
# define getcwd __getcwd
# ifndef stpcpy
#  define stpcpy __stpcpy
# endif
# define tfind __tfind
#else
# if !defined HAVE_GETCWD
char *getwd ();
#  define getcwd(buf, max) getwd (buf)
# else
#  if VMS
#   define getcwd(buf, max) (getcwd) (buf, max, 0)
#  else
char *getcwd ();
#  endif
# endif
# ifndef HAVE_STPCPY
static char *stpcpy (char *dest, const char *src);
# endif
# ifndef HAVE_MEMPCPY
static void *mempcpy (void *dest, const void *src, size_t n);
# endif
#endif

/* Use a replacement if the system does not provide the `tsearch' function
   family.  */
#if defined HAVE_TSEARCH || defined _LIBC
# include <search.h>
#else
# define tsearch libintl_tsearch
# define tfind libintl_tfind
# define tdelete libintl_tdelete
# define twalk libintl_twalk
# include "tsearch.h"
#endif

#ifdef _LIBC
# define tsearch __tsearch
#endif

/* Amount to increase buffer size by in each try.  */
#define PATH_INCR 32

/* The following is from pathmax.h.  */
/* Non-POSIX BSD systems might have gcc's limits.h, which doesn't define
   PATH_MAX but might cause redefinition warnings when sys/param.h is
   later included (as on MORE/BSD 4.3).  */
#if defined _POSIX_VERSION || (defined HAVE_LIMITS_H && !defined __GNUC__)
# include <limits.h>
#endif

#ifndef _POSIX_PATH_MAX
# define _POSIX_PATH_MAX 255
#endif

#if !defined PATH_MAX && defined _PC_PATH_MAX
# define PATH_MAX (pathconf ("/", _PC_PATH_MAX) < 1 ? 1024 : pathconf ("/", _PC_PATH_MAX))
#endif

/* Don't include sys/param.h if it already has been.  */
#if defined HAVE_SYS_PARAM_H && !defined PATH_MAX && !defined MAXPATHLEN
# include <sys/param.h>
#endif

#if !defined PATH_MAX && defined MAXPATHLEN
# define PATH_MAX MAXPATHLEN
#endif

#ifndef PATH_MAX
# define PATH_MAX _POSIX_PATH_MAX
#endif

/* Pathname support.
   ISSLASH(C)           tests whether C is a directory separator character.
   IS_ABSOLUTE_PATH(P)  tests whether P is an absolute path.  If it is not,
                        it may be concatenated to a directory pathname.
   IS_PATH_WITH_DIR(P)  tests whether P contains a directory specification.
 */
#if defined _WIN32 || defined __WIN32__ || defined __CYGWIN__ || defined __EMX__ || defined __DJGPP__
  /* Win32, Cygwin, OS/2, DOS */
# define ISSLASH(C) ((C) == '/' || (C) == '\\')
# define HAS_DEVICE(P) \
    ((((P)[0] >= 'A' && (P)[0] <= 'Z') || ((P)[0] >= 'a' && (P)[0] <= 'z')) \
     && (P)[1] == ':')
# define IS_ABSOLUTE_PATH(P) (ISSLASH ((P)[0]) || HAS_DEVICE (P))
# define IS_PATH_WITH_DIR(P) \
    (strchr (P, '/') != NULL || strchr (P, '\\') != NULL || HAS_DEVICE (P))
#else
  /* Unix */
# define ISSLASH(C) ((C) == '/')
# define IS_ABSOLUTE_PATH(P) ISSLASH ((P)[0])
# define IS_PATH_WITH_DIR(P) (strchr (P, '/') != NULL)
#endif

/* Whether to support different locales in different threads.  */
#if defined _LIBC || HAVE_USELOCALE || defined IN_LIBGLOCALE
# define HAVE_PER_THREAD_LOCALE
#endif

/* This is the type used for the search tree where known translations
   are stored.  */
struct known_translation_t
{
  /* Domain in which to search.  */
  const char *domainname;

  /* The category.  */
  int category;

#ifdef HAVE_PER_THREAD_LOCALE
  /* Name of the relevant locale category, or "" for the global locale.  */
  const char *localename;
#endif

#ifdef IN_LIBGLOCALE
  /* The character encoding.  */
  const char *encoding;
#endif

  /* State of the catalog counter at the point the string was found.  */
  int counter;

  /* Catalog where the string was found.  */
  struct loaded_l10nfile *domain;

  /* And finally the translation.  */
  const char *translation;
  size_t translation_length;

  /* Pointer to the string in question.  */
  union
    {
      char appended[ZERO];  /* used if domain != NULL */
      const char *ptr;      /* used if domain == NULL */
    }
  msgid;
};

gl_rwlock_define_initialized (static, tree_lock)

/* Root of the search tree with known translations.  */
static void *root;

/* Function to compare two entries in the table of known translations.  */
static int
transcmp (const void *p1, const void *p2)
{
  const struct known_translation_t *s1;
  const struct known_translation_t *s2;
  int result;

  s1 = (const struct known_translation_t *) p1;
  s2 = (const struct known_translation_t *) p2;

  result = strcmp (s1->domain != NULL ? s1->msgid.appended : s1->msgid.ptr,
		   s2->domain != NULL ? s2->msgid.appended : s2->msgid.ptr);
  if (result == 0)
    {
      result = strcmp (s1->domainname, s2->domainname);
      if (result == 0)
	{
#ifdef HAVE_PER_THREAD_LOCALE
	  result = strcmp (s1->localename, s2->localename);
	  if (result == 0)
#endif
	    {
#ifdef IN_LIBGLOCALE
	      result = strcmp (s1->encoding, s2->encoding);
	      if (result == 0)
#endif
		/* We compare the category last (though this is the cheapest
		   operation) since it is hopefully always the same (namely
		   LC_MESSAGES).  */
		result = s1->category - s2->category;
	    }
	}
    }

  return result;
}

/* Name of the default domain used for gettext(3) prior any call to
   textdomain(3).  The default value for this is "messages".  */
const char _nl_default_default_domain[] attribute_hidden = "messages";

#ifndef IN_LIBGLOCALE
/* Value used as the default domain for gettext(3).  */
const char *_nl_current_default_domain attribute_hidden
     = _nl_default_default_domain;
#endif

/* Contains the default location of the message catalogs.  */
#if defined __EMX__ && !defined __KLIBC__
extern const char _nl_default_dirname[];
#else
# ifdef _LIBC
extern const char _nl_default_dirname[];
libc_hidden_proto (_nl_default_dirname)
# endif
const char _nl_default_dirname[] = LOCALEDIR;
# ifdef _LIBC
libc_hidden_data_def (_nl_default_dirname)
# endif
#endif

#ifndef IN_LIBGLOCALE
/* List with bindings of specific domains created by bindtextdomain()
   calls.  */
struct binding *_nl_domain_bindings;
#endif

/* Prototypes for local functions.  */
static char *plural_lookup (struct loaded_l10nfile *domain,
			    unsigned long int n,
			    const char *translation, size_t translation_len)
     internal_function;

#ifdef IN_LIBGLOCALE
static const char *guess_category_value (int category,
					 const char *categoryname,
					 const char *localename)
     internal_function;
#else
static const char *guess_category_value (int category,
		
		 const char *categoryname)
     internal_function;
#endif

#ifdef _LIBC
# include "../locale/localeinfo.h"
# define category_to_name(category) \
  _nl_category_names.str + _nl_category_name_idxs[category]
#else
static const char *category_to_name (int category) internal_function;
#endif
#if (defined _LIBC || HAVE_ICONV) && !defined IN_LIBGLOCALE
static const char *get_output_charset (struct binding *domainbinding)
     internal_function;
#endif


/* For those losing systems which don't have `alloca' we have to add
   some additional code emulating it.  */
#ifdef HAVE_ALLOCA
/* Nothing has to be done.  */
# define freea(p) /* nothing */
# define ADD_BLOCK(list, address) /* nothing */
# define FREE_BLOCKS(list) /* nothing */
#else
struct block_list
{
  void *address;
  struct block_list *next;
};
# define ADD_BLOCK(list, addr)						      \
  do {									      \
    struct block_list *newp = (struct block_list *) malloc (sizeof (*newp));  \
    /* If we cannot get a free block we cannot add the new element to	      \
       the list.  */							      \
    if (newp != NULL) {							      \
      newp->address = (addr);						      \
      newp->next = (list);						      \
      (list) = newp;							      \
    }									      \
  } while (0)
# define FREE_BLOCKS(list)						      \
  do {									      \
    while (list != NULL) {						 truct block_list *old = list;					      \
      list = list->next;						      \
      free (old->address);						      \
      free (old);							      \
    }									      \
  } while (0)
# undef alloca
# define alloca(size) (malloc (size))
# define freea(p) free (p)
#endif	/* have alloca */


#ifdef _LIBC
/* List of blocks allocated for translations.  */
typedef struct transmem_list
{
  struct transmem_list *next;
  char data[ZERO];
} transmem_block_t;
static struct transmem_list *transmem_list;
#else
typedef unsigned char transmem_block_t;
#endif


/* Names for the libintl functions are a problem.  They must not clash
   with existing names and they should follow ANSI C.  But this source
   code is also used in GNU C Library where the names have a __
   prefix.  So we have to make a difference here.  */
#ifdef _LIBC
# define DCIGETTEXT __dcigettext
#else
# define DCIGETTEXT libintl_dcigettext
#endif

/* Lock variable to protect the global data in the gettext implementation.  */
gl_rwlock_define_initialized (, _nl_state_lock attribute_hidden)

/* Checking whether the binaries runs SUID must be done and glibc provides
   easier methods therefore we make a difference here.  */
#ifdef _LIBC
# define ENABLE_SECURE __libc_enable_secure
# define DETERMINE_SECURE
#else
# ifndef HAVE_GETUID
#  define getuid() 0
# endif
# ifndef HAVE_GETGID
#  define getgid() 0
# endif
# ifndef HAVE_GETEUID
#  define geteuid() getuid()
# endif
# ifndef HAVE_GETEGID
#  define getegid() getgid()
# endif
static int enable_secure;
# define ENABLE_SECURE (enable_secure == 1)
# define DETERMINE_SECURE \
  if (enable_secure == 0)						      \
    {									      \
      if (getuid () != geteuid () || getgid () != getegid ())		      \
	enable_secure = 1;						      \
      else								      \
	enable_secure = -1;						      \
    }
#endif

/* Get the function to evaluate the plural expression.  */
#include "eval-plural.h"

/* Look up MSGID in the DOMAINNAME message catalog for the current
   CATEGORY locale and, if PLURAL is nonzero, search over string
   depending on the plural form determined by N.  */
#ifdef IN_LIBGLOCALE
char *
gl_dcigettext (const char *domainname,
	       const char *msgid1, const char *msgid2,
	       int plural, unsigned long int n,
	       int category,
	       const char *localename, const char *encoding)
#else
char *
DCIGETTEXT (const char *domainname, const char *msgid1, const char *msgid2,
	    int plural, unsigned long int n, in    te