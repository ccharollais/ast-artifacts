#! /bin/bash

for e in /home/ast-proj/everything/aflpp/Os/*; do
    if [[ -d "${e}" ]]  && [[ ! $(basename $e) == *"aflpp"* ]]; then
        cd "${e}";
        /bin/bash compute_coverage.sh;
        cd /home/ast-proj/everything/aflpp/Os;
    fi
done
