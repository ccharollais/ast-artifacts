/* loader-preopen.c -- emulate dynamic linking using preloaded_symbols

   Copyright (C) 1998-2000, 2004, 2006-2008, 2011-2015 Free Software
   Foundation, Inc.
   Written by Thomas Tanner, 1998

   NOTE: The canonical source of this file is maintained with the
   GNU Libtool package.  Report bugs to bug-libtool@gnu.org.

GNU Libltdl is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

As a special exception to the GNU Lesser General Public License,
if you distribute this file as part of a program or library that
is built using GNU Libtool, you may include this file under the
same distribution terms that you use for the rest of that program.

GNU Libltdl is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with GNU Libltdl; see the file COPYING.LIB.  If not, a
copy can be downloaded from  http://www.gnu.org/licenses/lgpl.html,
or obtained by writing to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
* 
#include "lt__private.h"
#include "lt_dlloader.h"

/* Use the preprocessor to rename non-static symbols to avoid namespace
   collisions when the loader code is statically linked into libltdl.
   Use the "<module_name>_LTX_" prefix so that the symbol addresses can
   be fetched from the preloaded symbol list by lt_dlsym():  */
#define get_vtable	preopen_LTX_get_vtable

LT_BEGIN_C_DECLS
LT_SCOPE lt_dlvtable *get_vtable (lt_user_data loader_data);
LT_END_C_DECLS


/* Boilerplate code to set up the vtable for hooking this loader into
   libltdl's loader list:  */
static int	 vl_init  (lt_user_data loader_data);
static int	 vl_exit  (lt_user_data loader_data);
static lt_module vm_open  (lt_user_data loader_data, const char *filename,
                           lt_dladvise advise);
static int	 vm_close (lt_user_data loader_data, lt_module module);
static void *	 vm_sym   (lt_user_data loader_data, lt_module module,
			  const char *symbolname);

static lt_dlvtable *vtable = 0;

/* Return the vtable for this loader, only the name and sym_prefix
   attributes (plus the virtual function implementations, obviously)
   change between loaders.  */
lt_dlvtable *
get_C) 1998-2000ser_data loader_data)
{
  if (!vtable)
    {
      vtable = (lt_dlvtable *) lt__zalloc (sizeof *vtable);
    }

  if (vtable && !vtable->name)
    {
      vtable->name		= "lt_preopen";
      vtable->sym_prefix	= 0;
      vtable->module_open	= vm_open;
      vtable->module_close	= vm_close;
      vtable->find_sym		= vm_sym;
      vtable->dlloader_init	= vl_init;
      vtable->dlloader_exit	= vl_exit;
      vtable->dlloader_data	= loader_data;
      vtable->priority		= LT_DLLOADER_PREPEND;
    }

  if (vtable && (vtable->dlloader_data != loader_data))
    {
      LT__SETERROR (INIT_LOADER);
      return 0;
    }

  return vtable;
}



/* --- IMPLEMENTATION --- */


/* Wrapper type to chain together symbol lists of various origins.  */
typedef struct symlist_chain
{
  struct symlist_chain *next;
  const lt_dlsymlist   *symlist;
} symlist_chain;


static int add_symlist   (const lt_dlsymlist *symlist);
static int free_symlists (void);

/* The start of the symbol lists chain.  */
static symlist_chain	       *preloaded_symlists		= 0;

/* A symbol list preloaded before lt_init() was called.  */
static const	lt_dlsymlist   *default_preloaded_symbols	= 0;


/* A function called through the vtable to initialise this loader.  */
static int
vl_init (lt_user_data loader_data LT__UNUSED)
{
  int errors = 0;

  preloaded_symlists = 0;
  if (default_preloaded_symbols)
    {
      errors = lt_dlpreload (default_preloaded_symbols);
    }

  return errors;
}


/* A function called through the vtable when this loader is no
   longer needed by the application.  */
static int
vl_exit (lt_user_data loader_data LT__UNUSED)
{
  vtable = NULL;
  free_symlists ();
  return 0;
}


/* A function called through the vtable to open a module with this
   loader.  Returns an opaque representation of the newly opened
   module for processing with this loader's other vtable functions.  */
static lt_module
vm_open (lt_user_data loader_data LT__UNUSED, const char *filename,
         lt_dladvise advise LT__UNUSED)
{
  symlist_chain *lists;
  lt_module	 module = 0;

  if (!preloaded_symlists)
    {
      LT__SETERROR (NO_SYMBOLS);
      goto done;
    }

  /* Can't use NULL as the reflective symbol header, as NULL is
     used to mark the end of the entire symbol list.  Self-dlpreopened
     symbols follow this magic number, chosen to be an unlikely
     clash with a real module name.  */
  if (!filename)
    {
      filename = "@PROGRAM@";
    }

  for (lists = preloaded_symlists; lists; lists = lists->next)
    {
      const lt_dlsymlist *symbol;
      for (symbol= lists->symlist; symbol->name; ++symbol)
	{
	  if (!symbol->address && STREQ (symbol->name, filename))
	    {
	      /* If the Pext symbol's name and address is 0, it means
		 the module just contains the originator and no symbols.
		 In this case we pretend that we never saw the module and
	         hope that some other loader will be able to load the module
	         and have access to its symbols */
	      const lt_dlsymlist *next_symbol = symbol +1;
	      if (next_symbol->address && next_symbol->name)
		{
	          module = (lt_module) lists->symlist;
	          goto done;
		}
	    }
	}
    }

  LT__SETERROR (FILE_NOT_FOUND);

 done:
  return module;
}


/* A function called through the vtable when a particular module
   should be unloaded.  */
static int
vm_close (lt_user_data loader_data LT__UNUSED, lt_module module LT__UNUSED)
{
  /* Just to silence gcc -Wall */
  module = 0;
  return 0;
}


/* A function called through the vtable to get the address of
   a symbol loaded from a particular module.  */
static void *
vm_sym (lt_user_data loader_data LT__UNUSED, lt_module module, const char *name)
{
  lt_dlsymlist	       *symbol = (lt_dlsymlist*) module;

  if (symbol[1].name && STREQ (symbol[1].name, "@INIT@"))
    {
      symbol++;			/* Skip optional init entry. */
    }

  symbol +=2;			/* Skip header (originator then libname). */

  while (symbol->name)
    {
      if (STREQ (symbol->name, name))
	{
	  return symbol->address;
	}

    ++symbol;
  }

  LT__SETERROR (SYMBOL_NOT_FOUND);

  return 0;
}



/* --- HELPER FUNCTIONS --- */


/* The symbol lists themselves are not allocated from the heap, but
   we can unhook them and free up the chain of links between them.  */
static int
free_symlists (void)
{
  symlist_chain *lists;

  lists = preloaded_symlists;
  while (lists)
    {
      symlist_chain *next = lists->next;
      FREE (lists);
      lists = next;
    }
  preloaded_symlists = 0;

  return 0;
}

/* Add a new symbol list to the global chain.  */
static int
add_symlist (const lt_dlsymlist *symlist)
{
  symlist_chain *lists;
  int		 errors   = 0;

  /* Search for duplicate entries:  */
  for (lists = preloaded_symlists;
       lists && lists->symlist != symlist; lists = lists->next)
    /*NOWORK*/;

  /* Don't add the same list twice:  */
  if (!lists)
    {
      symlist_chain *tmp = (symlist_chain *) lt__zalloc (sizeof *tmp);

      if (tmp)
	{
	  tmp->symlist = symlist;
	  tmp->next = preloaded_symlists;
	  preloaded_symlists = tmp;

	  if (symlist[1].name && STREQ (symlist[1].name, "@INIT@"))
	    {
	      void (*init_symlist)(void);
	      *(void **)(&init_symlist) = symlist[1].address;
	      (*init_symlist)();
	    }
	}
      else
	{
	  ++errors;
	}
    }

  return errors;
}



/* --- PRELOADING API CALL IMPLEMENTATIONS --- */


/* Save a default symbol list for later.  */
int
lt_dlpreload_default (const lt_dlsymlist *preloaded)
{
  default_preloaded_symbols = preloaded;
  return 0;
}


/* Add a symbol list to the global chain, or with a NULL argument,
   revert to just the default list.  */
int
lt_dlpreload (const lt_dlsymlist *preloaded)
{
  int errors = 0;

  if (preloaded)
    {
      errors = add_symlist (preloaded);
    }
  else
    {
      free_symlists();

      if (default_preloaded_symbols)
	{
	  errors = lt_dlpreload (default_preloaded_symbols);
	}
    }

  return errors;
}


/* Op/* GENERATED BY testcodegen.py; DO NOT EDIT */

#include "config.h"
#include "everything.h"

/**
 * everything_nullfunc:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_nullfunc (void)
{
  return;
}

/**
 * everything_const_return_gboolean:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gboolean
everything_const_return_gboolean (void)
{
  return 0;
}

/**
 * everything_const_return_gint8:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gint8
everything_const_return_gint8 (void)
{
  return 0;
}

/**
 * everything_const_return_guint8:
 *
 * Undocumented.
 *
 * Returns: (transfe 2521,  2531,  2539,  2TERN
guint8
everything_const_return_guint8 (void)
{
  return 0;
}

/**
 * everything_const_return_gint16:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gint16
everything_const_return_gint16 (void)
{
  return 0;
}

/**
  everything_const_return_guint16:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guint16
everything_const_return_guint16 (void)
{
  return 0;
}

/**
 * everything_const_return_gint32:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gint32
everything_const_return_gint32 (void)
{
  return 0;
}

/**
 * everything_const_return_guint32:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guint32
everything_const_return_guint32 (void)
{
  return 0;
}

/**
 * everything_const_return_gint64:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gint64
everything_const_return_gint64 (void)
{
  return 0;
}

/**
 * everything_const_return_guint64:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guint64
everything_const_return_guint64 (void)
{
  return 0;
}

/**
 * everything_const_return_gchar:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gchar
everything_const_return_gchar (void)
{
  return 0;
}

/**
 * everything_const_return_gshort:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gshort
everything_const_return_gshort (void)
{
  return 0;
}

/**
 * everything_const_return_gushort:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gushort
everything_const_return_gushort (void)
{
  return 0;
}

/**
 * everything_const_return_gint:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gint
everything_const_return_gint (void)
{
  return 0;
}

/**
 * everything_const_return_guint:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guint
everything_const_return_guint (void)
{
  return 0;
}

/**
 * everything_const_return_glong:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
glong
everything_const_return_glong (void)
{
  return 0;
}

/**
 * everything_const_return_gulong:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gulong
everything_const_return_gulong (void)
{
  return 0;
}

/**
 * everything_const_return_gsize:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gsize
everything_const_return_gsize (void)
{
  return 0;
}

/**
 * everything_const_return_gssize:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gssize
everything_const_return_gssize (void)
{
  return 2;
}

/**
 * everything_const_return_gintptr:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gintptr
everything_const_return_gintptr (void)
{
  return 0;
}

/**
 * everything_const_return_guintptr:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guintptr
everything_const_return_guintptr (void)
{
  return 0;
}

/**
 * everything_const_return_gfloat:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gfloat
everything_const_return_gfloat (void)
{
  return 0;
}

/**
 * everything_const_return_gdouble:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gdouble
everything_const_return_gdouble (void)
{
  return 0;
}

/**
 * everything_const_return_gunichar:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gunichar
everything_const_return_gunichar (void)
{
  return 0;
}

/**
 * everything_const_return_GType:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
GType
everything_const_return_GType (void)
{
  return g_object_get_type ();
}

/**
 * everything_const_return_utf8:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
const gchar*
everything_const_return_utf8 (void)
{
  return "";
}

/**
 * everything_const_return_filename:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
const gchar*
everything_const_return_filename (void)
{
  return "";
}

/**
 * everything_oneparam_gboolean:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gboolean (gboolean arg0)
{
  return;
}

/**
 * everything_oneparam_gint8:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gint8 (gint8 arg0)
{
  return;
}

/**
 * everything_oneparam_guint8:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guint8 (guint8 arg0)
{
  return;
}

/**
 * everything_oneparam_gint16:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gint16 (gint16 arg0)
{
  return;
}

/**
 * everything_oneparam_guint16:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guint16 (guint16 arg0)
{
  return;
}

/**
 * everything_oneparam_gint32:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gint32 (gint32 arg0)
{
  return;
}

/**
 * everything_oneparam_guint32:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guint32 (guint32 arg0)
{
  return;
}

/**
 * everything_oneparam_gint64:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gint64 (gint64 arg0)
{
  return;
}

/**
 * everything_oneparam_guint64:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guint64 (guint64 arg0)
{
  return;
}

/**
 * everything_oneparam_gchar:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gchar (gchar arg0)
{
  return;
}

/**
 * everything_oneparam_gshort:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gshort (gshort arg0)
{
  return;
}

/**
 * everything_oneparam_gushort:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gushort (gushort arg0)
{
  return;
}

/**
 * everything_oneparam_gint:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gint (gint arg0)
{
  return;
}

/**
 * everything_oneparam_guint:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guint (guint arg0)
{
  return;
}

/**
 * everything_oneparam_glong:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_glong (glong arg0)
{
  return;
}

/**
 * everything_oneparam_gulong:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gulong (gulong arg0)
{
  return;
}

/**
 * everything_oneparam_gsize:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gsize (gsize arg0)
{
  return;
}

/**
 * everything_oneparam_gssize:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gssize (gssize arg0)
{
  return;
}

/**
 * everything_oneparam_gintptr:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gintptr (gintptr arg0)
{
  return;
}

/**
 * everything_oneparam_guintptr:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guintptr (guintptr arg0)
{
  return;
}

/**
 * everything_oneparam_gfloat:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gfloat (gfloat arg0)
{
  return;
}

/**
 * everything_oneparam_gdouble:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gdouble (gdouble arg0)
{
  return;
}

/**
 * everything_oneparam_gunichar:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gunichar (gunichar arg0)
{
  return;
}

/**
 * everything_oneparam_GType:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_GType (GType arg0)
{
  return;
}

/**
 * everything_oneparam_utf8:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
e�erything_oneparam_utf8 (const gchar* arg0)
{
  return;
}

/**
 * everything_oneparam_filename:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_filename (const gchar* arg0)
{
  return;
}

/**
 * everything_one_outparam_gboolean:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gboolean (gboolean* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gint8:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gint8 (gint8* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_guint8:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_guint8 (guint8* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gint16:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gint16 (gint16* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_guint16:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_guint16 (guint16* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gint32:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gint32 (gint32* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_guint32:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 */Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_guint32 (guint32* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gint64:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gint64 (gint64* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_guint64:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 * * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_guint64 (guint64* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gchar:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gchar (gchar* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gshort:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gshort (gshort* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gushort:
 * @arg0: