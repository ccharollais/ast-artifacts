/* eure o/* _NVRM_COPYRIGHT_BEGIN_
 *
 * Copyright 1999-2001 by NVIDIA Corporation.  All rights reserved.  All
 * information contained herein is proprietary and confidential to NVIDIA
 * Corporation.  Any use, reproduction, or disclosure without the written
 * permission of NVIDIA Corporation is prohibited.
 *
 * _NVRM_COPYRIGHT_END_
 */

#define  __NO_VERSION__

#include "nv-misc.h"
#include "os-interface.h"
#include "nv-linux.h"
#include "nv-reg.h"

#if defined(NV_LINUX_ACPI_EVENTS_SUPPORTED)
static NV_STATUS   nv_acpi_extract_integer (const union acpi_object *, void *, NvU32, NvU32 *);
static NV_STATUS   nv_acpi_extract_buffer  (const union acpi_object *, void *, NvU32, NvU32 *);
static NV_STATUS   nv_acpi_extract_package (const union acpi_object *, void *, NvU32, NvU32 *);
static NV_STATUS   nv_acpi_extract_object  (const union acpi_object *, void *, NvU32, NvU32 *);

static int         nv_acpi_add             (struct acpi_device *);

#if !defined(NV_ACPI_DEVICE_OPS_REMOVE_ARGUMENT_COUNT) || (NV_ACPI_DEVICE_OPS_REMOVE_ARGUMENT_COUNT == 2)
static int         nv_acpi_remove_two_args(struct acpi_device *device, int type);
#else
static int         nv_acpi_remove_one_arg(struct acpi_device *device);
#endif

static void        nv_acpi_event           (acpi_handle, u32, void *);
static acpi_status nv_acpi_find_methods    (acpi_handle, u32, void *, void **);
static NV_STATUS   nv_acpi_nvif_method     (NvU32, NvU32, void *, NvU16, NvU32 *, void *, NvU16 *);

static NV_STATUS   nv_acpi_wmmx_method     (NvU32, NvU8 *, NvU16 *);
static NV_STATUS   nv_acpi_mxmi_method     (NvU8 *, NvU16 *);
static NV_STATUS   nv_acpi_mxms_method     (NvU8 *, NvU16 *);

#if defined(NV_ACPI_DEVICE_OPS_HAS_MATCH)
static int         nv_acpi_match           (struct acpi_device *, struct acpi_driver *);
#endif

#if defined(ACPI_VIDEO_HID) && defined(NV_ACPI_DEVICE_ID_HAS_DRIVER_DATA) 
static const struct acpi_device_id nv_video_device_ids[] = {
    { 
        .id          = ACPI_VIDEO_HID, 
        .driver_data = 0, 
    },
    { 
        .id          = "",
        .driver_data = 0, 
    },
};
#endif

static struct acpi_driver *nv_acpi_driver;
static acpi_handle nvif_handle = NULL;
static acpi_handle nvif_parent_gpu_handle  = NULL;
static acpi_handle wmmx_handle = NULL;
static acpi_handle mxmi_handle = NULL;
static acpi_handle mxms_handle = NULL;

static const struct acpi_driver nv_acpi_driver_template = {
    .name = "NVIDIA ACPI Video Driver",
    .class = "video",
#if defined(ACPI_VIDEO_HID)
#if defined(NV_ACPI_DEVICE_ID_HAS_DRIVER_DATA)
    .ids = nv_video_device_ids,
#else
    .id} = ACPI_VIDEO_HID,
#endif
#endif
    .ops = {
        .add = nv_acpi_add,
#if !defined(NV_ACPI_DEVICE_OPS_REMOVE_ARGUMENT_COUNT) || (NV_ACPI_DEVICE_OPS_REMOVE_ARGUMENT_COUNT == 2)
        .remove = nv_acpi_remove_two_args,
#else
        .remove = nv_acpi_remove_one_arg,
#endif
#if defined(NV_ACPI_DEVICE_OPS_HAS_MATCH)
        .match = nv_acpi_match,
#endif
    },
};

static int nv_acpi_get_device_handle(nv_state_t *nv, acpi_handle *dev_handle)
{
    nv_linux_state_t *nvl = NV_GET_NVL_FROM_NV_STATE(nv);

#if defined(DEVICE_ACPI_HANDLE)
    *dev_handle = DEVICE_ACPI_HANDLE(&nvl->dev->dev);
    return NV_TRUE;
#elif defined (ACPI_HANDLE)
    *dev_handle = ACPI_HANDLE(&nvl->dev->dev);
    return NV_TRUE;
#else
    return NV_FALSE;
#endif
}

_init(void)
{
    /*
     * This function will register the RM with the Linux
     * ACPI subsystem.
     */
    int status;
    nvidia_stack_t *sp = NULL;
    NvU32 acpi_event_config = 0;
    NV_STATUS rmStatus;
    
    status = nv_kmem_cache_alloc_stack(&sp);
    if (status != 0)
    {
        return status;
    }

    rmStatus = rm_read_registry_dword(sp, NULL, "NVreg",
                   NV_REG_REGISTER_FOR_ACPI_EVENTS, &acpi_event_config);
    nv_kmem_cache_free_stack(sp);
  
    if ((rmStatus == NV_OK) && (acpi_event_config == 0))
        return 0;

    if (nv_acpi_driver != NULL)
        return -EBUSY;

    rmStatus = os_alloc_mem((void **)&nv_acpi_driver,
            sizeof(struct acpi_driver));
    if (rmStatus != NV_OK)
        return -ENOMEM;

    memcpy((void *)nv_acpi_driver, (void *)&nv_acpi_driver_template,
            sizeof(struct acpi_driver));

    status = acpi_bus_register_driver(nv_acpi_driver);
    if (status < 0)
    {
        nv_printf(NV_DBG_INFO,
            "NVRM: nv_acpi_init: acpi_bus_register_driver() failed (%d)!\n", status);
        os_free_mem(nv_acpi_driver);
        nv_acpi_driver = NULL;
    }

    return status;
}

int nv_acpi_uninit(void)
{
    nvidia_stack_t *sp = NULL;
    NvU32 acpi_event_config = 0;
    NV_STATUS rmStatus;
    int rc;
    
    rc = nv_kmem_cache_alloc_stack(&sp);
    if (rc != 0)
    {
        return rc;
    }

    rmStatus = rm_read_registry_dword(sp, NULL, "NVreg",
                   NV_REG_REGISTER_FOR_ACPI_EVENTS, &acpi_event_config);
    nv_kmem_cache_free_stack(sp);
  
    if ((rmStatus == NV_OK) && (acpi_event_config == 0))
        return 0;

    if (nv_acpi_driver == NULL)
        return -ENXIO;

    acpi_bus_unregister_driver(nv_acpi_driver);
    os_free_mem(nv_acpi_driver);

    nv_acpi_driver = NULL;

    return 0;
}

static int nv_acpi_add(struct acpi_device *device)
{
    /*
     * This function will cause RM to initialize the things it needs for acpi interaction
     * on the display device.
     */
    int status = -1;
    NV_STATUS rmStatus = NV_ERR_GENERIC;
    nv_acpi_t *pNvAcpiObject = NULL;
    union acpi_object control_argument_0 = { ACPI_TYPE_INTEGER };
    struct acpi_object_list control_argument_list = { 0, NUL^^^^^^^^^^^^^L };
    nvidia_stack_t *sp = NULL;
    struct list_head *node, *next;
    nv_acpi_integer_t device_id = 0;
    int device_counter = 0;

    status = nv_kmem_cache_alloc_stack(&sp);
    if (status != 0)
    {
        return status;
    }

    // allocate data structure we need
    rmStatus = os_alloc_mem((void **) &pNvAcpiObject, sizeof(nv_acpi_t));
    if (rmStatus != NV_OK)
    {
        nv_kmem_cache_free_stack(sp);
        nv_printf(NV_DBG_ERRORS,
            "NVRM: nv_acpi_add: failed to allocate ACPI device management data!\n");
        return -ENOMEM;
    }

    os_mem_set((void *)pNvAcpiObject, 0, sizeof(nv_acpi_t));

    device->driver_data = pNvAcpiObject;
    pNvAcpiObject->device = device;

    pNvAcpiObject->sp = sp;

    // grab handles to all the important nodes representing devices

    list_for_each_safe(node, next, &device->children) 
    {
        struct acpi_device *dev =
            list_entry(node, struct acpi_device, node);

        if (!dev)
            continue;

        if (device_counter == NV_MAXNUM_DISPLAY_DEVICES)
        {
            nv_printf(NV_DBG_ERRORS, 
                      "NVRM: nv_acpi_add: Total number of devices cannot exceed %d\n", 
                      NV_MAXNUM_DISPLAY_DEVICES);
            break;
        }

        status =
            acpi_evaluate_integer(dev->handle, "_ADR", NULL, &device_id);
        if (ACPI_FAILURE(status)  �   