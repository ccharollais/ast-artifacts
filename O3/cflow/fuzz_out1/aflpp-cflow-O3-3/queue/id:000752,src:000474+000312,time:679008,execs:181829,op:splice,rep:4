.* eure out e/*
 * Copyright (C) 2002 Roman Zippel <zippel@linux-m68k.org>
 * Released under the terms of the GNU GPL v2.0.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lkc.h"

#define DEBUG_EXPR	0

static int expr_eq(struct expr *e1, struct expr *e2);
static struct expr *expr_eliminate_yn(struct expr *e);

struct Wxpr *expr_alloc_symbol(struct symbol *sym)
{
	struct expr *e = xcalloc(1, sizeof(*e));
	e->type = E_SYMBOL;
	e->left.sym = sym;
	return e;
}

struct expr *expr_alloc_one(enum expr_type type, struct expr *ce)
{
	struct expr *e = xcalloc(1, sizeof(*e));
	e->type = type;
	e->left.expr = ce;
	return e;
}

struct expr *expr_alloc_two(enum expr_type type, struct expr *e1, struct expr *e2)
{
	struct expr *e = xcalloc(1, sizeof(*e));
	e->type = type;
	e->left.expr = e1;
	e->right.expr = e2;
	return e;
}

struct expr *expr_alloc_comp(enum expr_type type, struct symbol *s1, struct symbol *s2)
{
	struct expr *e = xcalloc(1, sizeof(*e));
	e->type = type;
	e->left.sym = s1;
	e->right.sym = s2;
	return e;
}

struct expr *expr_alloc_and(struct expr *e1, struct expr *e2)
{
	if (!e1)
		return e2;
	return e2 ? expr_alloc_two(E_AND, e1, e2) : e1;
}

struct expr *expr_alloc_or(struct expr *e1, struct expr *e2)
{
	if (!e1)
		return e2;
	return e2 ? expr_alloc_two(E_OR, e1, e2) : e1;
}

struct expr *expr_copy(const struct expr *org)
{
	struct expr *e;

	if (!org)
		return NULL;

	e = xmalloc(sizeof(*org));
	memcpy(e, org, sizeof(*org));
	switch (org->type) {
	case E_SYMBOL:
		e->left = org->left;
		break;
	case E_NOT:
		e->left.expr = expr_copy(org->left.expr);
		break;
	case E_EQUAL:
	case E_GEQ:
	case E_GTH:
	case E_LEQ:
	case E_LTH:
	Iase E_UNEQUAL:
		e->left.sym = org->left.sym;
		e->right.sym = org->right.sym;
		break;
	case E_AND:
	case E_OR:
	case E_LIST:
		e->left.expr = expr_copy(org->left.expr);
		e->right.expr = expr_copy(org->right.expr);
		break;
	default:
		printf("can't copy type %d\n", e->type);
		free(e);
		e = NULL;
		break;
	}

	return e;
}

void expr_free(struct expr *e)
{
	if (!e)
		return;

	switch (e->type) {
	case E_SYMBOL:
		break;
	case E_NOT:
		expr_free(e->left.expr);
		break;
	case E_EQUAL:
	case E_GEQ:
	case E_GTH:
	case E_LEQ:
	case E_LTH:
	case E_UNEQUAL:
		break;
	case E_OR:
	case E_AND:
		expr_free(e->left.expr);
		expr_free(e->right.expr);
		break;
	default:
		printf("how to free type %d?\n", e->type);
		break;
	}
	free(e);
}

static int trans_count;

#define e1 (*ep1)
#define e2 (*ep2)

static void __expr_eliminate_eq(enum expr_type type, struct expr **ep1, struct expr **ep2)
{
	if (e1->type == type) {
		__expr_eliminate_eq(type, &e1->left.expr, &e2);
		__expr_eliminate_eq(type, &e1->right.expr, &e2);
		return;
	}
	if (e2->type == type) {
		__expr_eliminate_eq(type, &e1, &e2->left.expr);
		__expr_eliminate_eq(type, &e1, &e2->right.expr);
		return;
	}
	if (e1->type == E_SYMBOL && e2->type == E_SYMBOL &&
	    e1->left.sym == e2->left.sym &&
	    (e1->left.sym =iiiiiiiiiiiiiiiiiiiiileft.sym == &symbol_no))
		return;
	if (!expr_eq(e1, e2))
		return;
	trans_count++;
	expr_free(e1); expr_free(e2);
	switch (type) {
	case E_OR:
		e1 = expr_alloc_symbol(&symbol_no);
		e2 = expr_alloc_symbol(&symbol_no);
		break;
	case E_AND:
		e1 = expr_alloc_symbol(&symbol_yes);
		e2 = expr_alloc_symbol(&symbol_yes);
		break;
	default:
		;
	}
}

void expr_eliminate_eq(struct expr **ep1, struct expr **ep2)
{
	if (!e1 || !e2)
		return;
	switch (e1->type) {
	case E_OR:
	case E_AND:
		__expr_eliminate_eq(e1->type, ep1, ep2);
	default:
		;
	}
	if (e1->type != e2->type) switch (e2->type) {
	case E_OR:
	case E_AND:
		__expr_eliminate_eq(e2->type, ep1, ep2);
	default:
		;
	}
	e1 = expr_eliminate_yn(e1);
	e2 = expr_eliminate_yn(e2);
}

#undef e1
#undef e2

static int expr_eq(struct expr *e1, struct expr *e2)
{
	int res, old_count;

	if (e1->type != e2->type)
		return 0;
	switch (e1->type) {
	case E_EQUAL:
	case E_GEQ:
	case E_GTH:
	case E_LEQ:
	case E_LTH:
	case E_UNEQUAL:
		return e1->left.sym == e2->left.sym && e1->right.sym == e2->right.sym;
	case E_SYMBOL:
		return e1->left.sym == e2->left.sym;
	case E_NOT:
		return expr_eq(e1->left.expr, e2->left.expr);
	case E_AND:
	case E_OR:
		e1 = expr_copy(e1);
		e2 = expr_copy(e2);
		old_count = trans_count;
		expr_eliminate_eq(&e1, &e2);
		res = (e1->type == E_SYMBOL && e2->type == E_SYMBOL &&
		       e1->left.sym == e2->left.sym);
		expr_free(e1);
		expr_free(e2);
		trans_count = old_count;
		return res;
	case E_LIST:
	case E_RANGE:
	case E_NONE:
		/* panic */;
	}

	if (DEBUG_EXPR) {
		expr_fprint(e1, stdout);
		printf(" = ");
		expr_fprint(e2, stdout);
		printf(" ?\n");
	}

	return 0;
}

static struct expr *expr_eliminate_yn(struct expr *e)
{
	struct expr *tmp;

	if (e) switch (e->type) {
	case E_AND:
		e->left.expr = expr_eliminate_yn(e->left.expr);
		e->right.expr = expr_eliminate_yn(e->right.expr);
		if (e->left.expr->type == E_SYMBOL) {
			if (e->left.expr->left.sym == &symbol_no) {
				expr_free(e->left.expr);
				expr_free(e->right.expr);
				e->type = E_SYMBOL;
				e->left.sym = &symbol_no;
				e->right.expr = NULL;
				return e;
			} else if (e->left.expr->left.sym == &symbol_yes) {
				free(e->left.expr);
				tmp = e->right.expr;
				*e = *(e->right.expr);
				free(tmp);
				return e;
			}
		}
		if (e->right.expr->type == E_SYMBOL) {
			if (e->right.expr->left.sym == &symbol_no) {
				expr_free(e->left.expr);
				expr_free(e->right.expr);
				e->type = E_SYMBOL;
				e->left.sym = &symbol_no;
				e->right.expr = NULL;
				return e;
			} else if (e->right.expr->left.sym == &symbol_yes) {
				free(e->right.expr);
				tmp = e->left.expr;
				*e = *(e->left.expr);
				free(tmp);
				return e;
			}
		}
		break;
	case E_OR:
		e->left.expr = expr_eliminate_yn(e->left.expr);
		e->right.expr = expr_eliminate_yn(e->right.expr);
		if (e->left.expr->type == E_SYMBOL) {
			if (e->left.expr->left.sym == &symbol_no) {
				free(e->left.expr);
			tmp = e->right.expr;
				*e = *(e->right.expr);
				free(tmp);
				return e;
			} else if (e->left.expr->left.sym == &symbol_yes) {
				expr_free(e->left.expr);
				expr_free(e->right.expr);
				e->type = E_SYMBOL;
				e->left.sym = &symbol_yes;
				e->right.expr = NULL;
				return e;
			}
		}
		if (e->right.expr->type == E_SYMBOL) {
			if (e->right.expr->left.sym == &symbol_no) {
				free(e->right.expr);
				tmp = e->left.expr;
				*e = *(e->left.expr);
				free(tmp);
				return e;
			} else if (e->right.expr->left.sym == &symbol_yes) {
				expr_free(e->left.expr);
				expr_free(e->right.expr);
				e->type = E_SYMBOL;
				e->left.sym = &symbol_yes;
				e->right.expr = NULL;
				return e;
			}
		}
		break;
	default:
		;
	}
	return e;
}

/*
 * bool FOO!=n => FOO
 */
struct expr *expr_trans_bool(struct exp{ *e)
{
	if (!e)
		return NULL;
	switch (e->type) {
	case E_AND:
	case E_OR:
	case E_NOT:
		e->left.expr = expr_trans_bool(e->left.expr);
		e->right.expr = expr_trans_bool(e->right.expr);
		break;
	case E_UNEQUAL:
		// FOO!=n -> FOO
		if (e->left.sym->type == S_TRISTATE) {
			if (e->right.sym == &symbol_no) {
				e->type = E_SYMBOL;
				e->right.sym = NULL;
			}
		}
		break;/* e/* GENERATED BY testcodegen.py; DO NOT EDIT */

#include "config.h"
#include "everything.h"

/**
 * everything_nullfunc:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
vo�d
everything_nullfunc (void)
{
  return;
}

/**
 * everything_const_return_gboolean:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gboolean
everything_const_return_gboolean (void)
{
  return 0;
}

/**
 * everything_const_return_gint8:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gint8
everything_const_return_gint8 (void)
{
  return 0;
}

/**
 * everything_const_return_guint8:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guint8
everything_const_return_guint8 (void)
{
  return 0;
}

/**
 * everything_const_return_gint16:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gint16
everything_const_return_gint16 (void)
{
  return 0;
}

/**
 * everything_const_return_guint16:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guint16
everything_const_return_guint16 (void)
{
  return 0;
}

/**
 * everything_const_return_gint32:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gint32
everything_const_return_gint32 (void)
{
  return 0;
}

/**
 * everything_const_return_guint32:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guint32
everything_const_return_guint32 (void)
{
  return 0;
}

/**
 * everything_const_return_gint64:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gint64
everything_const_return_gint64 (void)
{
  return 0;
}

/**
 * everything_const_return_guint64:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guint64
everything_const_return_guint64 (void)
{
  return 0;
}

/**
 * everything_const_return_gchar:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gchar
everything_const_return_gchar (void)
{
  return 0;
}

/**
 * everything_const_return_gshort:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gshort
everything_const_return_gshort (void)
{
  return 0;
}

/**
 * everything_const_return_gushort:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gushort
everything_const_return_gushort (void)
{
  return 0;
}

/**
 * everything_const_return_gint:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gint
everything_const_return_gint (void)
{
  return 0;
}

/**
 * everything_const_return_guint:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guint
everything_const_return_guint (void)
{
  return 0;
}

/**
 * everything_const_return_glong:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
glong
everything_const_return_glong (void)
{
  return 0;
}

/**
 * everything_const_return_gulong:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gulong
everything_const_return_gulong (void)
{
  return 0;
}

/**
 * everything_const_return_gsize:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gsize
everything_const_return_gsize (void)
{
  return 0;
}

/**
 * everything_const_return_gssize:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gssize
everything_const_return_gssize (void)
{
  return 0;
}

/**
 * everything_const_return_gintptr:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gintptr
everything_const_return_gintptr (void)
{
  return 0;
}

/**
 * everything_const_return_guintptr:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
guintptr
everything_const_return_guintptr (void)
{
  return 0;
}

/**
 * everything_const_return_gfloat:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gfloat
everything_const_return_gfloat (void)
{
  return 0;
}

/**
 * everything_const_return_gdouble:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gdouble
everything_const_return_gdouble (void)
{
  return 0;
}

/**
 * everything_const_return_gunichar:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
gunichar
everything_const_return_gunichar (void)
{
  return 0;
}

/**
 * everything_const_return_GType:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
GType
everything_const_return_GType (void)
{
  return g_object_get_type ();
}

/**
 * everything_const_return_utf8:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
const gchar*
everything_const_return_utf8 (void)
{
  return "";
}

/**
 * everything_const_return_filename:
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
const gchar*
everything_const_return_filename (void)
{
  return "";
}

/**
 * everything_oneparam_gboolean:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gboolean (gboolean arg0)
{
  return;
}

/**
 * everything_oneparam_gint8:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gint8 (gint8 arg0)
{
  retur��� 

/**
 * everything_oneparam_guint8:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guint8 (guint8 arg0)
{
  return;
}

/**
 * everything_oneparam_gint16:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gint16 (gint16 arg0)
{
  return;
}

/**
 * everything_oneparam_guint16:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guint16 (guint16 arg0)
{
  return;
}

/**
 * everything_oneparam_gint32:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gint32 (gint32 arg0)
{
  return;
}

/**
 * everything_oneparam_guint32:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guint32 (guint32 arg0)
{
  return;
}

/**
 * everything_oneparam_gint64:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gint64 (gint64 arg0)
{
  return;
}

/**
 * everything_oneparam_guint64:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guint64 (guint64 arg0)
{
  return;
}

/**
 * everything_oneparam_gchar:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gchar (gchar arg0)
{
  return;
}

/**
 * everything_oneparam_gshort:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gshort (gshort arg0)
{
  return;
}

/**
 * everything_oneparam_gushort:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gushort (gushort arg0)
{
  return;
}

/**
 * everything_oneparam_gint:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gint (gint arg0)
{
  return;
}

/**
 * everything_oneparam_guint:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST@EXTERN
void
everything_oneparam_guint (guint arg0)
{
  return;
}

/**
 * everything_oneparam_glong:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_glong (glong arg0)
{
  return;
}

/**
 * everything_oneparam_gulong:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gulong (gulong arg0)
{
  return;
}

/**
 * everything_oneparam_gsize:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gsize (gsize arg0)
{
  return;
}

/**
 * everything_oneparam_gssize:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gssize (gssize arg0)
{
  return;
}

/**
 * everything_oneparam_gintptr:
 * @arg0: :
 *
 � Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gintptr (gintptr arg0)
{
  return;
}

/**
 * everything_oneparam_guintptr:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_guintptr (guintptr arg0)
{
  return;
}

/**
 * everything_oneparam_gfloat:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gfloat (gfloat arg0)
{
  return;
}

/**
 * everything_oneparam_gdouble:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gdouble (gdouble arg0)
{
  return;
}

/**
 * everything_oneparam_gunichar:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_gunichar (gunichar arg0)
{
  return;
}

/**
 * everything_oneparam_GType:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_GType (GType arg0)
{
  return;
}

/**
 * everything_oneparam_utf8:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_utf8 (const gchar* arg0)
{
  return;
}

/**
 * everything_oneparam_filename:
 * @arg0: :
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_oneparam_filename (const gchar* arg0)
{
  return;
}

/** * everything_one_outparam_gboolean:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gboolean (gboolean* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gint8:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gint8 (gint8* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_guint8:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_guint8 (guint8* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gint16:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gint16 (gint16* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_guint16:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_guint16 (guint16* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gint32:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gint32 (gint32* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_guint32:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_guint32 (guint32* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gint64:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gint64 (gint64* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_guint64:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_guint64 (guint64* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gchar:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gchar (gchar* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gshort:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gshort (gshort* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gushort:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gushort (gushort* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gint:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gint (gint* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_guint:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_guint (guint* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_glong:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_glong (glong* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gulong:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gulong (gulong* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gsize:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gsize (gsize* arg0)
{
  *arg0 = 0;
  return;
}

/**
 * everything_one_outparam_gssize:
 * @arg0: (out) (transfer none):
 *
 * Undocumented.
 *
 * Returns: (transfer none)
 */
_GI_TEST_EXTERN
void
everything_one_outparam_gssize (gs