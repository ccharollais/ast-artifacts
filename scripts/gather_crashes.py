#! /usr/bin/python3

import os

opts=["O0", "O1", "O2", "O3", "Os"]
progs=["objdump", "tcpdump", "cflow", "lame", "wav2swf", "flvmeta"]
f="aflpp"
reps=3
instances=4

root="~/everything/aflpp/"
dest="/home/ast-proj/everything/aflpp/crashes_res"

for o in opts:
    for p in progs:
        for r in range(reps):
            for i in range(instances):
                os.system(f"cp {root}/{o}/{p}/fuzz_out{r}/{f}-{p}-{o}-{i}/fuzzer_stats {dest}/{o}-{p}-rep{r}-instance{i}-fuzzer_stats")


