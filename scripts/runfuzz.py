#! /usr/bin/python3

import platform as pf


prog_to_opt= {
    "objdump": ["O0", "O1", "O2", "O3", "Os"],
    "tcpdump": ["O0", "O1", "O2", "O3", "Os"],
    "cflow": ["O0", "O1", "O2", "O3", "Os"],
    "lame": ["O0", "O1", "O2", "O3", "Os"],
    "wav2swf": ["O0", "O1", "O2", "O3", "Os"],
    "flvmeta": ["O0", "O1", "O2", "O3", "Os"],
}

root="/home/ast-proj/everything/aflpp"
t=5
delta=65

if "macOS" in pf.platform():
    filedest=""
if "Linux" in pf.platform():
    filedest=root + '/'


with open(filedest + "all_fuzz.sh", 'w') as outfile:
    outfile.write("#! /bin/bash\n\n")
    for p in prog_to_opt.keys():
        for o in prog_to_opt[p]:
            for nrep in range(3):
                outfile.write(f"echo \"(cd {root}/{o}/{p}; ./fuzz_rep{nrep}.sh)\" | at now + {t} minutes\n")
                t += delta


print(f"fuzzing script written at: {filedest}all_fuzz.sh")


            
