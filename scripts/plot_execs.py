import argparse
import logging
import os
import re
from numpy import median
from pandas import DataFrame
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

logging.basicConfig(level=logging.INFO)

# dataformat:
#   opt source  coverage
# 0 -03 objdump 0.516
# 1 -Os objdump 0.031
# 2 -O3 tcpdump 0.023
# 3 -O3 tcpdump 0.021
# ....

testdict = {
    ""
}

parser = argparse.ArgumentParser(
    description="Generate a barchart based on coverage data files contained in a given directory"
)
parser.add_argument(
    "-p", "--path", help="path to the directory containing the coverage data", type=str, required=True
)
parser.add_argument(
    "-t", "--title", help="plot title to use", type=str, required=True
)
parser.add_argument(
    "-o", "--output", help="filename to use for the generated plot", type=str, required=True
)

def scan_directory(p:str) -> DataFrame:
    logging.info(f'reading coverage data from {p}...')
    df = DataFrame(columns=['source','opt', 'rep', 'nexecs'])
    for f in os.listdir(p):
        # parse coverage data
        params = f.split('-')
        opt = params[0]
        source = params[1]
        rep = int(params[2][-1])
        inst = params[3][-1]
        filepath = os.path.join(p, f)
        inputf = open(filepath)
        content = inputf.read()
        line = re.findall(r'execs_done        : \d+', content)[0]
        nexecs = int(re.findall(r'(\d+)', line)[0])
        inputf.close()
        logging.debug(f'found file {f} with params opt {opt}, src {source}, rep {rep}, inst {inst} and {nexecs}')
        df.loc[len(df.index)] = [source, opt, rep, nexecs]

    df = df.groupby(['source', 'opt', 'rep'])['nexecs'].sum().reset_index()

    return df

def cov_chart_abs(df:DataFrame, title:str, filename:str):
    sns.barplot(x="source", y="nexecs", hue="opt", hue_order=['O0','O1','O2','O3','Os'], data=df, estimator=median, palette='pastel', ci=None)

    f = f'{filename}_abs.png'
    plt.xlabel('utility')
    plt.ylabel('Number of crashes')
    logging.info(f'storing the barchart with nexecs values to {f}...')
    plt.savefig(f)
    plt.clf()

def cov_chart_rel(df:DataFrame, title:str, filename:str):
    # use O0 as a baseline
    o0_covs = df[df['opt'].isin(['O0'])].groupby('source').median('nexecs')
    df = df[df['opt'] != 'O0']

    # compute coverage relative to O0
    for i, row in df.iterrows():
        o0_cov = o0_covs.loc[row['source']]['nexecs']
        df.loc[i, 'nexecs'] = row['nexecs'] - o0_cov

    sns.barplot(x="source", y="nexecs", hue="opt", hue_order=['O0','O1','O2','O3','Os'], data=df, estimator=median, palette='pastel', ci=None)

    f = f'{filename}_rel.png'
    plt.xlabel('utility')
    plt.ylabel('Relative difference to -O0')
    logging.info(f'storing the barchart with nuber of crashes to {f}...')
    plt.savefig(f)
    plt.clf()

def detailed_latex_table(df:DataFrame):
    """ prints a table of detailed coverage info to output as latex code """
    header_opt = ' & '.join(['Optimization'] + [f'\multicolumn{{3}}{{|c|}}{{{o}}}' for o in ['-O0', '-O1', '-O2', '-O3', '-Os']])
    header_rep = ' & '.join(['Repetition'] + [' & '.join([str(i+1) for i in range(3)]) for _ in ['-O0', '-O1', '-O2', '-O3', '-Os']])
    header = header_opt + '\\\\\n' + header_rep
    all_sources_info = []
    grouped_by_source = df.groupby('source')
    for source, source_data in grouped_by_source:
        source_info = f'\\texttt{{{source}}}'
        for opt in ['O0', 'O1', 'O2', 'O3', 'Os']:
            for rep in range(3):
                for _, row in source_data[source_data['opt'] == opt][source_data['rep'] == rep].iterrows():
                    source_info = source_info + f' & {float(row["nexecs"]) / 1000000:.2f}'
        all_sources_info.append(source_info)
    content = '\\\\\n\hline\n'.join([header] + all_sources_info)
    logging.info(f'''
        Generated latex code:

        \\begin{{table}}
            \centering
            \\begin{{tabular}}{{ | c | c c c | c c c | c c c | c c c | c c c | }}
                \hline
                {content} \\\\
                \hline
            \end{{tabular}}
            \caption{{Detailed overview over number of crashes.}}
            \label{{tab:detailed number of crashes}}
        \end{{table}}
    ''')

if __name__ == "__main__":
    args = parser.parse_args()
    data = scan_directory(args.path)
    cov_chart_abs(data.drop(['rep'], axis=1), args.title, args.output)
    cov_chart_rel(data.drop(['rep'], axis=1), args.title, args.output)
    detailed_latex_table(data)