#! /bin/bash

ORIG="/home/ast-proj/everything"

for name in */*/showmap/*.stdout;do 
    N=$name
    parts=(${name//\// });
    name=${parts[${#parts[@]} - 1]};
    opt=${parts[${#parts[@]} - 3]};
    prog=${parts[${#parts[@]} - 4]};

    cp "${N}" "${ORIG}/aflpp/coverage_res/${prog}-${opt}-${name}";
done
