import argparse
import os
from tqdm import tqdm


parser = argparse.ArgumentParser(
    description="""Generates a hash of the contents of a folder"""
)

parser.add_argument(
    "-p", "--path", help="path to the directory whose content is hashed", type=str, default='.', 
)

def hash_folder(path):
    lof = []

    rt = os.path.abspath(path)

    for root, _, files in os.walk(path):
        for file in files:
            pth_from_rt = os.path.abspath(os.path.join(root,file)).split(rt)[-1][1:]
            pth = os.path.join(root,file)
            lof.append((pth_from_rt, pth))

    lof.sort(key= lambda pair: pair[0])


    h = ''
    for f in tqdm(lof):
        with open(f[1],'rb') as file:
            content = file.read()
            next = f[0].encode() + content + h.encode()
            h = hex(hash(next))

    return f"hash of {path} : {h}"


if __name__ == "__main__":
    args = parser.parse_args()
    print(hash_folder(args.path))



