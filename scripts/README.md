In this folder you will find the scripts we used and what is necessary to reproduce our results.

## tldr;

```{bash}
 # Choose a folder where to reproduce the experiment, navigate to it.
$ ORIG=$(pwd);
 # Copy ast-setup into $ORIG
 # Copy res/ into $ORIG
$ ./ast-setup -a -r;
$ sudo ast/aflpp/O0/aflpp/ast-system-config;
$ cd ast/aflpp/O0/cflow;
$ cat fuzz_rep0.sh;
$ ${ORIG}/ast/aflpp/O0/aflpp/afl-fuzz \ 
$ -M aflpp-cflow-O0-0 -V 3600 -i seeds \
$ -o fuzz_out0 -- program_src/src/cflow @@;
 # Troubleshoot the errors displayed by afl-fuzz 
 # Once afl-fuzz runs properly continue.
$ rm -rf fuzz_out0/*;
$ cd $ORIG;
 # Copy runfuzz.py to $ORIG/ast/aflpp
 # Edit it so root=${ORIG}/ast/aflpp
$ cd ${ORIG}/ast/aflpp;
$ ./runfuzz.py
$ ./all_fuzz.sh
 # Go on weekend, once it is all done continue
$ ./all_covs.sh
 # Do a lunch break
 # Copy gather_covs.sh to $ORIG/ast/aflpp
 # Edit it so root="$ORIG/aflpp/"
 # Copy gather_crashes.py to $ORIG/ast/aflpp
 # Edit it so root="$ORIG/ast/aflpp/" and dest "$ORIG/ast/aflpp/crashes_res"
$ mkdir crashes_res
$ mkdir coverage_res
$ chmod 755 gather_covs.sh
$ chmod 755 gather_crashes.py
$ ./gather_covs.sh
$ ./gather_crashes.py
 # Copy plot_coverage.py, plot_crashes.py, and plot_execs.py to $ORIG/aflpp
$ mkdir plots
 # Make sure seaborn, pandas and numpy are available or install them with pip
$ python3 plot_coverage.py -p coverage_res -t coverage -o plots/coverage
$ python3 plot_crashes.py -p coverage_res -t crashes -o plots/crashes
$ python3 plot_execs.py -p coverage_res -t execs -o plots/execs
```

## Step 1: Setup

On an Ubuntu machine, first choose a folder to run the experiment in, copy `ast-setup` and `res/` into it. Then, run the script `ast-setup`. This script installs the dependencies for `AFL++` and all the targets we tested. Several options are available. Below is the usage information the script provides:
```
Usage ast-setup: ast-setup { -a | [ --fuzzer=FUZZER..
                                    --program=PROGRAM..
                                    --opt-strat=OPT_STRAT..
                                    --fuzz-time=SECONDS
                                    --num-repetitions=n ] }
                           [ -r ] [--root-dir=ROOT_DIR]
                           [ --only-cov-scr ]
Description:
        - Generates the setup for fuzzing different programs
        under various optimisation and for different fuzzers.
        - Performs a dry run by default, to actually run use -r.
Parameters:
    -h, --help:
        Displays this usage information
    -r:
        Performs a actual run.
    -a, --all:
        Setup for all combinations of fuzzers, programs, and optimisation
        strategies. If this flag is present, specific fuzzer, program,
        and optimisation strategies provided as extra arguments are ignored.
        Default values:
            Fuzzers: aflpp
            Programs: objdump tcpdump cflow lame wav2swf flvmeta
            Optimisation strategies: O0 O1 O2 O3 Os
            Fuzz time: 3600 seconds
            Number of repetitions: 3
    --root-dir=ROOT_DIR:
        Specifies the root working directory. The default is $(pwd)/ast.
        It must either a relative path from the script directory or an absolute
        path.
    --fuzzer=FUZZER:
        Define the fuzzer(s) to be used, to define several fuzzers use the
        parameter multiple times. If not specified, the default is used.
    --program=PROGRAM:
        Define the program(s) to be fuzzed, to define several programs use the
        parameter multiple times. If not specified, the default is used.
    --opt-strat=OPT_STRAT:
        Define the optimisation strategy(ies) the program(s) to be fuzzed have
        to be compiled with, to define several startegies use the parameter
        multiple times. If not specified, the default is used.
    --fuzz-time=SECONDS
        Sets the fuzzing time to SECONDS before timing out. If not specified
        the default is used.
    --num-repetitions=n
        Sets the number of times the binary is fuzzed. If not specified the
        default is used.
    --only-cov-scr
        Only generates the coverage computing scripts for the specified fuzzers
        programs and optimisation strategies.
```

To reproduce all our tests run `ast-setup -a -r --root-dir=$PATH` with `$PATH` being the path to your destination folder of choice.

The script also detects the number of cores available in your system and automatically generates fuzzing scripts taking advantage of all of them.

## Step 2: Fuzzing

### Step 2.1: System setup for fuzzing

#### 2.1.1

Execute `sudo ast-system-config`

#### 2.1.2 

Navigate to the folder containing one target. It is located at `$PATH/aflpp/$OPT/$PROGRAM` with 
- `$PATH` being the root folder you chose in Step 1
- `$OPT` being one of the optimization levels you chose in Step 1
- `$PROGRAM` being one of the target you chose in Step 1.

Once there, execute `cat fuzz_rep0.sh`, select the command `screen` is to execute in the first line you see and execute it. 

If `afl-fuzz` displays any error message, follow the instructions and repeat until `afl-fuzz` starts fuzzing the target. Then empty the eventual fuzzing outputs generated with `rm -rf fuzz_out0/*`.


### Step 2.1: Handsfree fuzzing

To avoid needing to start each fuzzing instance by hand, one after the other (which can be really cumbersome), you can use the script `runfuzz`. You first need to edit it slightly to satisfy your needs.

First copy `runfuzz.py` to `$PATH/aflpp` (`$PATH` being the root folder you chose in Step 1).

Then, edit the `prog_to_fuzz` dictionary to specify which targets you want to automatically fuzz. Edit the `root` variable and set it to `$PATH/aflpp`.

Go to `$PATH/aflpp` and execute `./runfuzz.sh`. This will generate the `all_fuzz.sh` script which you need to make executable (with `chmod 755 all_fuzz.sh` for instance) and execute like so: `./all_fuzz.sh`.

Now you can go on weekend while everything is being fuzzed. While it is fuzzing you can check the status of it by running `screen -ls` which outputs the current target being fuzzed which you can compare to the contents of `all_fuzz.sh` to get an idea of where it is at.

### Step 2.1: Selective manual fuzzing

If you prefer to fuzz each target manually, just navigate to the folder of the desired target with `cd $PATH/aflpp/$OPT/$PROGRAM` with 
- `$PATH` being the root folder you chose in Step 1
- `$OPT` being one of the optimization levels you chose in Step 1
- `$PROGRAM` being one of the target you chose in Step 1.

Then execute `fuzz_rep$NREP.sh` one after the other with `$NREP`being the sequence from 0 to (not including) the number of repetitions you chose in Step 1.


## Step 3: Computing coverage

If you manually fuzzed only a few targets navigate to their respective directories and execute `./copute_coverage.sh`. The coverage results are computed and stored in `showmap`.

If you fuzzed all the targets using `all_fuzz.sh`, navigate to `$PATH/aflpp` and execute `./all_covs.sh` (`$PATH` being the root folder you chose in Step 1).

## Step 4: Gathering the results


### Step 4.1: gathering the coverages:

Copy `gather_covs.sh` into `$PATH/aflpp`, and edit it so `ORIG=$PATH`(`$PATH` being the root folder you chose in Step 1).
Then make it executable, navigate to `$PATH/aflpp`, and execute `./gather_covs.sh`. The coverage results are gathered and copied into `coverage_res/`.

### Step 4.1: gathering the crashes:

Copy `gather_crashes.sh` into `$PATH/aflpp`, and edit it so `root="$PATH/aflpp/"` and `dest="$PATH/aflpp/crashes_res"`(`$PATH` being the root folder you chose in Step 1)
Then make it executable, navigate to `$PATH/aflpp`, and execute `./gather_crashes.sh`. The coverage results are gathered and copied into `crashes_res/`.

## Step 5: Make the results usable

First, make sure `seaborn`, `pandas`, and `numpy` are installed, otherwise install them with `pip`.

Then, copy `plot_coverage.py`,`plot_crashes.py`, and `plot_execs.py` into `$PATH/aflpp`(`$PATH` being the root folder you chose in Step 1). Create the directory `$PLOT_OUT` where you want the plots to be generated and choose a suitable title for all the plots (`$TITLE`). Then navigate `$PATH/aflpp` and execute the three scripts with:
```
$ python3 plot_coverage.py -p coverage_res -t $TITLE -o /path/to/$PLOT_OUT/coverage
$ python3 plot_crashes.py -p crashes_res -t $TITLE -o /path/to/$PLOT_OUT/crashes
$ python3 plot_execs.py -p crashes_res -t $TITLE -o /path/to/$PLOT_OUT/execs
``` 

The scripts will output to the standard output a latex table containing the values gathered, generate the plots and store them in the `$PLOT_OUT` folder.
