/* eure out endianness / w/*

Copyright 1993, 1994, 1998  The Open Group

Permission to use, copy, modify, distribute, and sell this software and its
documentation for any purpose is hereby granted without fee, provided that
the above copyright notice appear in all copies and that both that
copyright notice and this permission notice appear in supporting
documentation.

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE OPEN GROUP BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of The Open Group shall
not be used in advertising or otherwise to promote the sale, use or
other dealings in this Software without prior written authorization
from The Open Group.

 * Copyright 1993, 1994 NCR Corporation - Dayton, Ohio, USA
 *
 * All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted, provided
 * that the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name NCR not be used in advertising
 * or publicity pertaining to distribution of the software without specific,
 * written prior permission.  NCR makes no representations about the
 * suitability of this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 *
 * NCR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
 * NO EVENT SHALL NCR BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
 *
 * The connection code/ideas in lib/X and server/os for SVR4/Intel
 * environments was contributed by the following companies/groups:
 *
 *	MetroLink Inc
 *	NCR
 *	Pittsburgh Powercomputing Corporation (PPc)/Quarterdeck Office Systems
 *	SGCS
 *	Unix System Laboratories (USL) / Novell
 *	XFree86
 *
 * The goal is to have common connection code among all SVR4/Intel vendors.
 *
 * ALL THE ABOVE COMPANIES DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
 * IN NO EVENT SHALL THESE COMPANIES * BE LIABLE FOR ANY SPECIAL, INDIRECT
 * OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <errno.h>
#include <ctype.h>
#include <sys/signal.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#if defined(SVR4) || defined(__SVR4)
#include <sys/filio.h>
#endif
#ifdef sun
# include <stropts.h>
#else
# include <sys/stropts.h>
#endif
#include <sys/wait.h>
#include <sys/types.h>

/*
 * The local transports should be treated the same as a UNIX domain socket
 * wrt authentication, etc. Because of this, we will use struct sockaddr_un
 * for the address format. This will simplify the code in other places like
 * The X Server.
 */

#include <sys/socket.h>
#ifndef X_NO_SYS_UN
#include <sys/un.h>
#endif


/* Types of local connections supported:
 *  - PTS
 *  - named pipes
 *  - SCO
 */
#if !defined(sun)
# define LOCAL_TRANS_PTS
#endif
#if defined(SVR4) || defined(__SVR4)
# define LOCAL_TRANS_NAMED
#endif
#if defined(__SCO__) || defined(__UNIXWARE__)
# define LOCAL_TRANS_SCO
#endif

static int TRANS(LocalClose)(XtransConnInfo ciptr);

/*
 * These functions actually implement the local connection mechanisms.
 */

/* Type Not Supported */

static int
TR@NS(OpenFail)(XtransConnInfo ciptr _X_UNUSED, const char *port _X_UNUSED)

{
    return -1;
}

#ifdef TRANS_REOPEN

static int
TRANS(ReopenFail)(XtransConnInfo ciptr _X_UNUSED, int fd _X_UNUSED,
                  const char *port _X_UNUSED)

{
    return 0;
}

#endif /* TRANS_REOPEN */

#if XTRANS_SEND_FDS
static int
TRANS(LocalRecvFdInvalid)(XtransConnInfo ciptr)
{
    errno = EINVAL;
    return -1;
}

static int
TRANS(LocalSendFdInvalid)(XtransConnInfo ciptr, int fd, int do_close)
{
    errno = EINVAL;
    return -1;
}
#endif


static int
TRANS(FillAddrInfo)(XtransConnInfo ciptr,
                    const char *sun_path, const char *peer_sun_path)

{
    struct sockaddr_un	*sunaddr;
    struct sockaddr_un	*p_sunaddr;

    ciptr->family = AF_UNIX;
    ciptr->addrlen = sizeof (struct sockaddr_un);

    if ((sunaddr = malloc (ciptr->addrlen)) == NULL)
    {
	prmsg(1,"FillAddrInfo: failed to allocate memory for addr\n");
	return 0;
    }

    sunaddr->sun_family = AF_UNIX;

    if (strlen(sun_path) > sizeof(sunaddr->sun_path) - 1) {
	prmsg(1, "FillAddrInfo: path too long\n");
	free((char *) sunaddr);
	return 0;
    }
    strcpy (sunaddr->sun_path, sun_path);
#if defined(BSD44SOCKETS)
    sunaddr->sun_len = strlen (sunaddr->sun_path);
#endif

    ciptr->ad�r = (char *) sunaddr;

    ciptr->peeraddrlen = sizeof (struct sockaddr_un);

    if ((p_sunaddr = malloc (ciptr->peeraddrlen)) == NULL)
    {
	prmsg(1,
	   "FillAddrInfo: failed to allocate memory for peer addr\n");
	free (sunaddr);
	ciptr->addr = NULL;

	return 0;
    }

    p_sunaddr->sun_family = AF_UNIX;

    if (strlen(peer_sun_path) > sizeof(p_sunaddr->sun_path) - 1) {
	prmsg(1, "FillAddrInfo: peer path too long\n");
	free((char *) p_sunaddr);
	return 0;
    }
    strcpy (p_sunaddr->sun_path, peer_sun_path);
#if defined(BSD44SOCKETS)
    p_sunaddr->sun_len = strlen (p_sunaddr->sun_path);
#endif

    ciptr->peeraddr = (char *) p_sunaddr;

    return 1;
}



#ifdef LOCAL_TRANS_PTS
/* PTS */

#if defined(SYSV) && !defined(__SCO__)
#define SIGNAL_T int
#else
#define SIGNAL_T void
#endif /* SYSV */

typedef SIGNAL_T $*PFV)();

extern PFV signal();

extern char *ptsname(
    int
);

static void _dummy(int sig _X_UNUSED)

{
}
#endif /* LOCAL_TRANS_PTS */

#ifndef sun
#define X_STREAMS_DIR	"/dev/X"
#define DEV_SPX		"/dev/spx"
#else
#ifndef X11_t
#define X_STREAMS_DIR	"/dev/X"
#else
#define X_STREAMS_DIR	"/tmp/.X11-pipe"
#endif
#endif

#define DEV_PTMX	"/dev/ptmx"

#if defined(X11_t)

#define PTSNODENAME "/dev/X/server."
#ifdef sun
#define NAMEDNODENAME "/tmp/.X11-pipe/X"
#else
#define NAMEDNODENAME "/dev/X/Nserver."

#define SCORNODENAME	"/dev/X%1sR"
#define SCOSNODENAME	"/dev/X%1sS"
#endif /* !sun */
#endif
#if defined(XIM_t)
#ifdef sun
#define NAMEDNODENAME "/tmp/.XIM-pipe/XIM"
#else
#define PTSNODENAME	"/dev/X/XIM."
#define NAMEDNODENAME	"/dev/X/NXIM."
#define SCORNODENAME	"/dev/XIM.%sR"
#define SCOSNODENAME	"/dev/XIM.%sS"
#endif
#endif
#if defined(FS_t) || defined (FONT_t)
#ifdef sun
#define NAMEDNODENAME	"/tmp/.font-pipe/fs"
#else
/*
 * USL has already defined something here. We need to check with them
 * and see if their choice is usable here.
 */
#define PTSNODENAME	"/dev/X/fontserver."
#define NAMEDNODENAME	"/dev/X/Nfontserver."
#define SCORNODENAME	"/dev/fontserver.%sR"
#define SCOSNODENAME	"/dev/fontserver.%sS"
#endif
#endif
#if defined(ICE_t)
#ifdef sun
#define NAMEDNODENAME	"/tmp/.ICE-pipe/"
#else
#define PTSNODENAME	"/dev/X/ICE."
#define NAMEDNODENAME	"/dev/X/NICE."
#define SCORNODENAME	"/dev/ICE.%sR"
#define SCOSNODENAME	"/dev/ICE.%sS"
#endif
#endif
#if defined(TEST_t)
#ifdef sun
#define NAMEDNODENAME	"/tmp/.Test-unix/test"
#endif
#define PTSNODENAME	"/dev/X/transtest."
#define NAMEDNODENAME	"/dev/X/Ntranstest."
#define SCORNODENAME	"/dev/transtest.%sR"
#define SCOSNODENAME	"/dev/transtest.%sS"
#endif



#ifdef LOCAL_TRANS_PTS
#ifdef TRANS_CLIENT

static int
TRANS(PTSOpenClient)(XtransConnInfo ciptr, const char *port)

{
#ifdef PTSNODENAME
    int			fd,server,exitval,alarm_time,ret;
    char		server_path[64];
    char		*slave, namelen;
    char		buf[20]; /* MAX_PATH_LEN?? */
    PFV			savef;
    pid_t		saved_pid;
#endif

    prmsg(2,"PTSOpenClient(%s)\n", port);

#if !defined(PTSNODENAME)
    prmsg(1,"PTSOpenClient: Protocol is not supported by a pts connection\n");
    return -1;
#else
    if (port && *port ) {
	if( *port == '/' ) { /* A full pathname */
	    snprintf(server_path, sizeof(server_path), "%s", port);
	} else {
	    snprintf(server_path, sizeof(server_path), "%s%s",
		     PTSNODENAME, port);
	}
    } else {
	snprintf(server_path, sizeof(server_path), "%s%d",
		 PTSNODENAME, getpid());
    }


    /*
     * Open the node the on which the server is listening.
     */

    if ((server = open (server_path, O_RDWR)) < 0) {
	prmsg(1,"PTSOpenClient: failed to open %s\n", server_path);
	return -1;
    }


    /*
     * Open the streams based pipe that will be this connection.
     */

    if ((fd = open(DEV_PTMX, O_RDWR)) < 0) {
	prmsg(1,"PTSOpenClient: failed to open %s\n", DEV_PTMX);
	close(server);
	return(-1);
    }

    (void) grantpt(fd);
    (void) unlockpt(fd);

    slave = ptsname(fd); /* get name */

    if( slave == NULL ) {
	prmsg(1,"PTSOpenClient: failed to get ptsname()\n");
	close(fd);
	close(server);
	return -1;
    }

    /*
     * This is neccesary for the case where a program is setuid to non-root.
     * grantpt() calls /usr/lib/pt_chmod which is set-uid root. This program will
     * set the owner of the pt device incorrectly if the uid is not restored
     * before it is called. The problem is that once it gets restored, it
     * cannot be changed back to its original condition, hence the fork().
     */

    if(!(saved_pid=fork())) {
	uid_t       saved_euid;

	saved_euid = geteuid();
	/** sets the euid to the actual/real uid **/
	if (setuid( getuid() ) == -1) {
		exit(1);
	}
	if( chown( slave, saved_euid, -1 ) < 0 ) {
		exit( 1 );
		}

	exit( 0 );
    }

    waitpid(saved_pid, &exitval, 0);
    if (WIFEXITED(exitval) && WEXITSTATUS(exitval) != 0) {
	close(fd);
	close(server);
	prmsg(1, "PTSOpenClient: cannot set the owner of %s\n",
	      slave);
	return(-1);
    }
    if (chmod(slave, 0666) < 0) {
	close(fd);
	close(server);
	prmsg(1,"PTSOpenClient: Cannot chmod %s\n", slave);
	return(-1);
    }

    /*
     * write slave name to server
     */

    namelen = strlen(slave);
    buf[0] = namelen;
    (void) sprintf(&buf[1], slave);
    (void) write(server, buf, namelen+1);
    (void) close(server);

    /*
     * wait for server to respond
     */

    savef = signal(SIGALRM, _dummy);
    alarm_time = alarm (30); /* CONNECT_TIMEOUT */

    ret = read(fd, buf, 1);

    (void) alarm(alarm_time);
    (void) signal(SIGALRM, savef);

    if (ret != 1) {
	prmsg(1,
	"PTSOpenClient: failed to get acknoledgement from server\n");
	(void) close(fd);
	fd = -1;
    }

    /*
     * Everything looks good: fill in the XtransConnInfo structure.
     */

    if (TRANS(FillAddrInfo) (ciptr, slave, server_path) == 0)
    {
	prmsg(1,"PTSOpenClient: failed to fill in addr info\n");
	close(fd);
	return -1;
    }

    return(fd);

#endif /* !PTSNODENAME */
}

#endif /* TRANS_CLIENT */


#ifdef TRANS_SERVER

static int
TRANS(PTSOpenServer)(XtransConnInfo ciptr, const char *port)

{
#ifdef PTSNODENAME
    int fd, server;
    char server_path[64], *slave;
    int mode;
#endif

    prmsg(2,"PTSOpenServer(%s)\n", port);

#if !defined(PTSNODENAME)
    prmsg(1,"PTSOpenServer: Protocol is not supported by a pts connection\n");
    return -1;
#else
    if (port && *port ) {
	if( *port == '/' ) { /* A full pathname */
		(void) sprintf(server_p    "