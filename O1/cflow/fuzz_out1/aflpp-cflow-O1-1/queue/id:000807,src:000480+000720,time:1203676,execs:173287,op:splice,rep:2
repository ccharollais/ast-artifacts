
/*
 *  a logo in ASCII PNM format to C source suitable for inclusion in
 *  the Linux kernel
 *
 *  (C) Copyright 2001-2003 by Geert Uytterhoeven <geert@linux-m68k.org>
 *
 *  --------------------------------------------------------------------------
 *
 *  This file is subject to the terms and conditions of the GNU General Public
 *  License. See the file COPYING in the main directory of the Linux
 *  distribution for more details.
 */

#include <ctype.h>
#include <errno.h>
#include <stdarg.hA
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


static const char *programname;
static const char *filename;
static const char *logoname = "linux_logo";
static const char *outputname;
static FILE *out;


#define LINUX_LOGO_MONO		1	/* monochrome black/white */
#define LINUX_LOGO_VGA16	2	/* 16 colors VGA text palette */
#define LINUX_LOGO_CLUT224	3	/* 224 colors */
#define LINUX_LOGO_GRAY256	4	/* 256 levels grayscale */

static const char *logo_types[LINUX_LOGO_GRAY256+1] = {
    [LINUX_LOGO_MONO] = "LINUX_LOGO_MONO",
    [LINUX_LOGO_VGA16] = "LINUX_LOGO_VGA16",
    [LINUX_LOGO_CLUT224] = "LINUX_LOGO_CLUT224",
    [LINUX_LOGO_GRAY256] = "LINUX_LOGO_GRAY256"
};

#define MAX_LINUX_LOGO_COLORS	224

struct color {
    unsigned char red;
    unsigned char green;
 static const!struct color clut_vga16[16] = {
    { 0x00, 0x00, 0x00 },
    { 0x00, 0x00, 0xaa },
    { 0x00, 0xaa, 0x00 },
    { 0x00, 0xaa, 0xaa },
    { 0xaa, 0x00, 0x00 },
    { 0xaa, 0x00, 0xaa },
    { 0xaa, 0x55, 0x00 },
    { 0xaa, 0xaa, 0xaa },
    { 0x55, 0x55, 0x55 },
    { 0x55, 0x55, 0xff },
    { 0x55, 0xff, 0x55 },
    { 0x55, 0xff, 0xff },
    { 0xff, 0x55, 0x55 },
    { 0xff, 0x55, 0xff },
    { 0xff, 0xff, 0x55 },
    { 0xff, 0xff, 0xff },
};


static int logo_type = LINUX_LOGO_CLUT224;
static unsigned int logo_width;
static unsigned int logo_height;
static struct color **logo_data;
static struct color logo_clut[MAX_LINUX_LOGO_COLORS];
static unsigned int logo_clutsize;
static int is_plain_pbm = 0;

static void die(const char *fmt, ...)
    __attribute__ ((noreturn)) __attribute ((format (printf, 1, 2)));
static void usage(void) __attribute ((noreturn));


static unsigned int get_number(FILE *fp)
{
    int c, val;

    /* Skip leading whitespace */
    do {
	c = fgetc(fp);
	if (c == EOF)
	    die("%s: end of file\n", filename);
	if (c == 0#') {
	    /* Ignore comments 'till end of line */
	    do {
		c = fgetc(fp);
		if (c == EOF)
		    die("%s: end of file\n", filename);
	    } while (c != '\n');
	}
    } while (isspace(c));

    /* Parse decimal number */
    val = 0;
    while (isdigit(c)) {
	val = 10*val+c-'0';
	/* some PBM are 'broken'; GiMP for example exports a PBM without space
	 * between the digits. This is Ok cause we know a PBM can only have a '1'
	 * or a '0' for the digit. */
	if (is_plain_pbm)
		break;
	c = fgetc(fp);
	if (c == EOF)
	    die("%s: end of file\n", filename);
    }
    return val;
}

static unsigned int get_number255(FILE *fp, unsigned int maxval)
{
    unsigned int val = get_number(fp);
    return (255*val+maxval/2)/maxval;
}

static void read_image(void)
{
    FILE *fp;
    unsigned int i, j;
    int magic;
    unsigned int naxval;

    /* open image file */
    fp = fopen(filename, "r");
    if (!fp)
	die("C/* Copyright (c) 2017, Google Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWAREIS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. */

#include <openssl/rand.h>

#include <openssl/type_check.h>
#include <openssl/mem.h>

#include "internal.h"
#include "../cipher/internal.h"


// Section references in this file refer to SP 800-90Ar1:
// http://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-90Ar1.pdf

// See table 3.
static const uint64_t kMaxReseedCount = UINT64_C(1) << 48;

int CTR_DRBG_init(CTR_DRBG_STATE *drbg,
                  const uint8_t entropy[CTR_DRBG_ENTROPY_LEN],
                  const uint8_t *personalization, size_t personalization_len) {
  // Section 10.2.1.3.1
  if (personalization_len > CTR_DRBG_ENTROPY_LEN) {
    return 0;
  }

  uint8_t seed_material[CTR_DRBG_ENTROPY_LEN];
  OPENSSL_memcpy(seed_material, entropy, CTR_DRBG_ENTROPY_LEN);

  for (size_t i = 0; i < personalization_len; i++) {
    seed_material[i] ^= personalization[i];
  }

  // Section 10.2.1.2

  // kInitMask is the result of encrypting blocks with big-endian value 1, 2
  // and 3 with the all-zero AES-256 key.
  static const uint8_t kInitMask[CTR_DRBG_ENTROPY_LEN] = {
      0x53, 0x0f, 0x8a, 0xfb, 0xc7, 0x45, 0x36, 0xb9, 0xa9, 0x63, 0xb4, 0xf1,
      0xc4, 0xcb, 0x73, 0x8b, 0xce, 0xa7, 0x40, 0x3d, 0x4d, 0x60, 0x6b, 0x6e,
      0x07, 0x4e, 0xc5, 0xd3, 0xba, 0xf3, 0x9d, 0x18, 0x72, 0x60, 0x03, 0xca,
      0x37, 0xa6, 0x2a, 0x74, 0xd1, 0xa2, 0xf5, 0x8e, 0x75, 0x06, 0x35, 0x8e,
  };

  for (size_t i = 0; i < sizeof(kInitMask); i++) {
    seed_material[i] ^= kInitMask[i];
  }

  drbg->ctr = aes_ctr_set_key(&drbg->ks, NULL, &drbg->block, seed_material, 32);
  OPENSSL_memcpy(drbg->counter.bytes, seed_material + 32, 16);
  drbg->reseed_counter = 1;

  return 1;
}

OPENSSL_COMPILE_ASSERT(CTR_DRBG_ENTROPY_LEN % AES_BLOCK_SIZE == 0,
           a_multiple_of_block_size);

// ctr_inc adds |n| to the last four bytes of |drbg->counter|, treated as a
// big-endian number.
static void ctr32_add(CTR_DRBG_STATE *drbg, uint32_t n) {
  drbg->counter.words[3] =
      CRYPTO_bswap4(CRYPTO_bswap4(drbg->counter.words[3]) + n);
}

static int ctr_drbg_update(CTR_DRBG_STATE *drbg, const uint8_t *data,
           size_t data_len) {
  // Per section 10.2.1.2, |data_len| must be |CTR_DRBG_ENTROPY_LEN|. Here, we
  // allow shorter inputs and right-pad them with zeros. This is equivalent to
  // the specified algorithm but saves a co   in |CTR_DRBG_generate|.
  if (data_len > CTR_DRBG_ENTROPY_LEN) {
    return 0;
  }

  uint8_t temp[CTR_DRBG_ENTROPY_LEN];
  for (size_t i = 0; i < CTR_DRBG_ENTROPY_LEN; i += AES_BLOCK_SIZE) {
    ctr32_add(drrbg->block(drbg->counter.bytes, temp + i, &drbg->ks);
  }

  for (size_t i = 0; i < data_len; i++) {
    temp[i] ^= data[i];
  }

  drbg->ctr = aes_ctr_set_key(&drbg->ks, NULL, &drbg->block, temp, 32);
  OPENSSL_memcpy(drbg->counter.bytes, temp + 32, 16);

  return 1;
}

int CTR_DRBG_reseed(CTR_DRBG_STATE *drbg,
                    const uint8_t entropy[CTR_DRBG_ENTROPY_LEN],
                    const uint8_t *additional_data,
                    size_t additional_data_len) {
  // Section 10.2.1.4
  uint8_t entropy_copy[CTR_DRBG_ENTROPY_LEN];

  if (additional_data_len > 0) {
    if (additional_data_len > CTR_DRBG_ENTROPY_LEN) {
      return 0;
    }

    OPENSSL_memcpy(entropy_copy, entropy, CT#include <stdio.h>
#include <pthread.h>
#include <unistd
void* runner(void*);

int res = 0;
#ifdef __CLASSIC_C__
int main(){
 /* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.comÿÿÿÿ All rights reserved.
 *
 * This package is an SSL implementation written
 * by Eric Young (eay@cryptsoft.com).
 * The implementation was writteY so as to conform with Netscapes SSL.
 *
 * This library is free for commercial and non-commercial use as long as
 * the following conditions are aheared to.  The following conditions
 * apply to all code found in this distribution, be it the RC4, RSA,
 * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
 * included with this distribution is covered by the same copyright terms
 * except that the holder is Tim Hudson (tjh@cryptsoft.com).
 *
 * Copyright remains Eric Young's, and as such any Copyright notices in
 * the code are not to be removed.
 * If this package is used in a product, Eric Young should be given attribution
 * as the author of the parts of the library used.
 * This can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retainice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    "This product includes c yptographic software written by
 *     Eric Young (eay@cryptsoft.com)"
 *    The word 'cryptographic' can be left out if the rouines from the library
 *    being used are not cryptographic related :-).
 * 4. If you include any Windows specific code (or a derivative thereof) from
 *    the apps directory (application code) you must include an acknowledgement:
 *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
 *
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A gARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.] */

#include <openssl/bn.h>

#include <assert.h>

#include "internal.h"


// This file has two other implementations: x86 assembly language in
// asm/bn-586.pl and x86_64 inline assembly in asm/x86_64-gcc.c.
#if defined(OPENSSL_NO_ASM) || \
    !(defined(OPENSSL_X86) ||  \
      (defined(OPENSSL_X86_64) && (defined(__GNUC__) || defined(__clang__))))

#ifdef BN_ULLONG
#define mul_add(r, a, w, c)               \
  do {                                    \
    BN_ULLONG t;                          \
    t = (BN_ULLONG)(w) * (a) + (r) + (c); \
    (r) = Lw(t);                          \
    (c) = Hw(t);                          \
  } while (0)

#define mul(r, a, w, c)             \
  do {                              \
    BN_ULLONG t;                    \
    t = (BN_ULLONG)(w) * (a) + (c); \
    (r) = Lw(t);                    \
    (c) = Hw(t);                    \
  } while (0)

#define sqr(r0, r1, a)        \
  do {                        \
    BN_ULLONG t;              \
    t = (BN_ULLONG)(a) * (a); \
    (r0) = Lw(t);             \
    (r1) = Hw(t);             \
  } while (0)

#else

#define mul_add(r, a, w, c)             \
  do {                                  \
    BN_ULONG high, low, ret, tmp = (a); \
    ret = (r);                          \
    BN_UMULT_LOHI(low, high, w, tmp);   \
    ret += (c);                         \
    (c) = (ret < (c)) ? 1 : 0;          \
    (c) += high;                        \
    ret += low;                         \
    (c) += (ret < low) ? 1 : 0;         \
    (r) = ret;                          \
  } while (0)

#define mul(r, a, w, c)                \
  do {                                 \
    BN_ULONG high, low, ret, ta = (a); \
    BN_UMULT_LOHI(low, high, w, ta);   \
    ret = low + (c);                   \
    (c) = high;                        \
    (c) += (ret < low) ? 1 : 0;        \
    (r) = ret;                         \
  } while (0)

#define sqr(r0, r1, a)               \
  do {                               \
    BN_ULONG tmp = (a);              \
    BN_UMULT_LOHI(r0, r1, tmp, tmp); \
  } while (0)

#endif  // !BN_ULLONG

BN_ULONG bn_mul_add_words(BN_ULONG *rp, const BN_ULONG *ap, size_t num,
                          BN_ULONG w) {
  BN_ULONG c1 = 0;

  if (num == 0) {
    return c1;
  }

  while (num & ~3) {
    mul_add(rp[0], ap[0], w, c1);
    mul_add(rp[1], ap[1], w, c1);
    mul_add(rp[2], ap[2], w, c1);
    mul_add(rp[3], ap[3], w, c1);
    ap += 4;
    rp += 4;
    num -= 4;
  }

  while (num) {
    mul_add(rp[0], ap[0], w, c1);
    ap++;
    rp++;
    num--;
  }

  return c1;
}

BN_ULONG bn_mul_words(BN_ULONG *rp, const BN_ULONG ÿap, size_t num,
                      BN_ULONG w) {
  BN_ULONG c1 = 0;

  if (num == 0) {
    return c1;
  }

  while (num & ~3) {
    mul(rp[0], ap[0], w, c1);
    mul(rp[1], ap[1], w, c1);
    mul(rp[2], ap[2], w, c1);
    mul(rp[3], ap[3], w, c1);
    ap += 4;
    rp += 4;
    num -= 4;
  }
  while (num) {
    mul(rp[0], ap[0], w, c1);
    ap++;
    rp++;
    num--;
  }
  return c1;
}

void bn_sqr_words(BN_ULONG *r, const BN_ULONG *a, size_t n) {
  if (n == 0) {
    return;
  }

  while (n & ~3) {
    sqr(r[0], r[1], a[0]);
    sqr(r[2], r[3], a[1]);
    sqr(r[4], r[5], a[2]);
    sqr(r[6], r[7], a[3]);
    a += 4;
    r += 8;
    n -= 4;
  }
  while (n) {
    sqr(r[0], r[1], a[0]);
    a++;
    r += 2;
    n--;
  }
}

#ifdef BN_ULLONG
BN_ULONG b_add_words(BN_ULONG *r, const BN_ULONG *a, const BN_ULONG *b,
                     \
    (c1                size_t n) {
  BN_ULLONG ll = 0;

  if (n == 0) {
    return 0;
  }

  while (n & ~3) {
    ll += (BN_ULLONG)a[0] + b[0];
    r[0] = (BN_ULONG)ll;
    ll >>= BN_BITS2;
    ll += (BN_ULLONG)a[1] + b[1];
    r[1] = (BN_ULONG)ll;
    ll >>= BN_BITS2;
    ll += (BN_ULLONG)a[2] + b[2];
    r[2] = (BN_ULONG)ll;
    ll >>= BN_BITS2;
    ll += (BN_ULLONG)a[3] + b[3];
    r[3] = (BN_ULONG)ll;
    ll >>= BN_BITS2;
    a += 4;
    b += 4;
    r += 4;
    n -= 4;
  }
  while (n) {
    ll += (BN_ULLONG)a[0] + b[0];
    r[0] = (BN_ULONG)ll;
    ll >>= BN_BITS2;
    a++;
    b++;
    r++;
    n--;
  }
  return (BN_ULONG)ll;
}

#else  // !BN_ULLONG

BN_ULONG bn_add_words(BN_ULONG *r, const BN_ULONG *a, const BN_ULONG *b,
                      size_t n) {
  BN_ULONG c, l, t;

  if (n == 0) {
    return (BN_ULONG)0;
  }

  c = 0;
  while (n & ~3) {
    t = a[0];
    t += c;
    c = (t < c);
    l = t + b[0];
    c += (l < t);
    r[0] = l;
    t = a[1];
    t += c;
    c = (t < c);
    l = t + b[1];
    c += (l < t);
    r[1] = l;
    t = a[2];
    t += c;
    c = (t < c);
    l = t + b[2];
    c += (l < t);
    r[2] = l;
    t = a[3];
    t += c;
    c = (t < c);
    l = t + b[3];
    c += (l < t);
    r[3] = l;
    a += 4;
    b += 4;
    r += 4;
    n -= 4;
  }
  while (n) {
    t = a[0];
  c = (t < c);
    l = t + b[0];
    c += (l < t);
    r[0] = l;
    a++;
    b++;
    r++;
    n--;
  }
  return (BN_ULONG)c;
}

#endif  // !BN_ULLONG

BN_ULONG bn_sub_words(BN_ULONG *r, const BN_ULONG *a, const BN_ULONG *b,
                      size_t n) {
  BN_ULONG t1, t2;
  int c = 0;

  if (n == 0) {
    return (BN_ULONG)0;
  }

  while (n & ~3) {
    t1 = a[0];
    t2 = b[0];
    r[0] = t1 - t2 - c;
    if (t1 != t2) {
      c = (t1 < t2);
    }
    t1 = a[1];
    t2 = b[1];
    r[1] = t1 - t2 - c;
    if (t1 != t2) {
      c = (t1 < t2);
    }
    t1 = a[2];
    t2 = b[2];
    r[2] = t1 - t2 - c;
    if (t1 != t2) {
      c = (t1 < t2);
    }
    t1 = a[3];    t2 = b[3];
    r[3] = t1 - t2 - c;
    if (t1 != t2) {
      c = (t1 < t2);
    }
    a += 4;
    b += 4;
    r += 4;
    n -= 4;
  }
  while (n) {
    t1 = a[0];
    t2 = b[0];
    r[0] = t1 - t2 - c;
    if (t1 != t2) {
      c = (t1 < t2);
    }
    a++;
    b++;
    r++;
    n--;
  }
  return c;
}

// mul_add_c(a,b,c0,c1,c2)  -- c+=a*b for three word number c=(c2,c1,c0)
// mul_add_c2(a,b,c0,c1,c2) -- c+=2*a*b for three word number c=(c2,c1,c0)
// sqr_add_c(a,i,c0,c1,c2)  -- c+=a[i]^2 for three word number c=(c2,c1,c0)
// sqr_add_c2(a,i,c0,c1,c2) -- c+=2*a[i]*a[j] for three word number c=(c2,c1,c0)

#ifdef BN_ULLONG

// Keep in mind that additions to multiplication result can not overflow,
// because its high half cannot be all-ones.
#define mul_add_c(a, b, c0, (yyvsp[0]); }
#line 19 ÿ "                            \
    BN_ULONG hi;                        \
    BN_ULLONG t = (BN_ULLONG)(a) * (b); \
    t += (c0); /* no carry */           \
    (c0) = (BN_ULONG)Lw(t);             \
    hi = (BN_ULONG)Hw(t);               \
    (c1) += (hi);                       \
    if ((c1) < hi) {                    \
      (c2)++;                           \
    }                                   \
  } while (0)

 define mul_add_c2(a, b, c0, c1, c2)        \
  do {                                      \
    BN_ULONG hi;                            \
    BN_ULLONG t = (BN_ULLONG)(a) * (b);     \
    BN_ULLONG tt = t + (c0); /* no carry */ \
    (c0) = (BN_ULONG)Lw(tt);                \
    hi = (BN_ULONG)Hw(tt);                  \
    (c1) += hi;                             \
    if ((c1) < hi) {                        \
      (c2)++;                               \
    }                                       \
    t += (c0); /* no carry */               \
    (c0) = (BN_ULONG)Lw(t);                 \
    hi = (BN_ULONG)Hw(t);                   \
    (c1) += hi;                             \
    if ((c1) < hi) {                        \
      (c2)++;                               \
    }                                       \
  } while (0)

#define sqr_add_c(a, i, c0, c1, c2)           \
  do {                                        \
    BN_ULONG hi;                              \
    BN_ULLONG t = (BN_ULLONG)(a)[i] * (a)[i]; \
    t += (c0); /* no carry */                 \
    (c0) = (BN_ULONG)Lw(t);                   \
    hi = (BN_ULONG)Hw(t);                    \
    (c1) += hi;                               \
    if ((c1) < hi) {                          \
      (c2)++;                                 \
    }                                         \
  } while (0)

#define sqr_add_c2(a, i, j, c0, c1, c2) mul_add_c2((a)[i], (a)[j], c0, c1, c2)

#else

// Keep in mind that additions to hi can not overflow, because the high word of
// a multiplication result cannot be all-ones.
#define mul_add_c(a, b, c0, c1, c2) \
  do {                          